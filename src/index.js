import React from 'react';
import ReactDOM from 'react-dom';

import {createStore ,applyMiddleware,combineReducers} from 'redux';
import {Provider} from 'react-redux';

import thunk from 'redux-thunk';
import logger from 'redux-logger';

import reducers from './reducersOfAll';
import App from './route';



import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux';

const history = createHistory();

const store = createStore(
    combineReducers({
        ...reducers,
        routing:routerReducer,
    }),
    applyMiddleware(routerMiddleware(history),thunk,logger)
);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
           <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));
