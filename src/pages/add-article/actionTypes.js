import keyMirror from 'keymirror';

export default keyMirror({
    ADD_REQUEST: null,
    ADD_SUCCESS: null,
    ADD_FAILURE: null,

    RELEASE_REQUEST: null,
    RELEASE_SUCCESS: null,
    RELEASE_FAILURE: null,
})