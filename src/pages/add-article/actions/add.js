import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function adding() {
    return {
        type: actionTypes.ADD_REQUEST
    };
}

function added(data) {
    return {
        type: actionTypes.ADD_SUCCESS,
        data,
    };
}

function addFail(error) {
    return {
        type: actionTypes.ADD_FAILURE,
        error,
    };
}

export default function addData(data) {

    return (dispatch) => {
        dispatch(adding());

        // dispatch(added())
        const token = getToken();
        req.postRequestDetail(token, config.addArticle, data).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                dispatch(added(responseJson.data))
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(addFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.retCode.message);
            }
        }).catch((err) => {
            dispatch(addFail(err));
            console.error(err);
        });
    }
}