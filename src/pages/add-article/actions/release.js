/**
 * Created by Richie on 2018/4/18
 */
import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function releasing() {
    return {
        type: actionTypes.RELEASE_REQUEST
    };
}

function released(data) {
    return {
        type: actionTypes.RELEASE_SUCCESS,
        data,
    };
}

function releaseFail(error) {
    return {
        type: actionTypes.RELEASE_FAILURE,
        error,
    };
}

export default function release(data) {

    return (dispatch) => {
        dispatch(releasing());
        const token = getToken();
        req.postRequestDetail(token, config.addArticleRelease, data).then((responseJson) => {

            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.error(responseJson.data);
                dispatch(released(responseJson.data));

            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(releaseFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.retCode.message);
            }
        }).catch((err) => {
            dispatch(releaseFail(err));
            console.error(err);
        });
    }

}