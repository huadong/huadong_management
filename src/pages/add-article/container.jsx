import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { adding,releasing } = state.addArticle.toJS();
    return {
        adding,releasing
    };
};

const mapDispatchToProps = dispatch => ({
    addArticle:(data) => {
        dispatch(actions.addArticle(data))
    },
    release:(data) => {
        dispatch(actions.release(data))
    }
});

class Container extends Component {
    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}

Container.propTypes = {
    addArticle:PropTypes.func.isRequired,
    release:PropTypes.func.isRequired,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);