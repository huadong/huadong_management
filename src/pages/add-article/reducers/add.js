import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.ADD_REQUEST:
            return state.merge({
                adding:false,
            });
        case actionTypes.ADD_SUCCESS: {
            return state.merge({
                adding:true,
            });
        }
        case actionTypes.ADD_FAILURE:
            return state.merge({
                adding:false,
            });
        default:
            return state;
    }
};