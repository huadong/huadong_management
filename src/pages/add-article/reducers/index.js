import { fromJS } from 'immutable';
import reduceReducers from 'reduce-reducers';

import addArticle from './add';
import release from './release';

const initialState = fromJS({
    adding:false,
    releasing:false
});

const reducer = reduceReducers(
    (state = initialState, action) => addArticle(state, action),
    (state = initialState, action) => release(state, action)
);

export default reducer;