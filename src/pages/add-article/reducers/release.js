/**
 * Created by Richie on 2018/4/18
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.ADD_REQUEST:
            return state.merge({
                releasing:false,
            });
        case actionTypes.ADD_SUCCESS: {
            return state.merge({
                releasing:true,
            });
        }
        case actionTypes.ADD_FAILURE:
            return state.merge({
                releasing:false,
            });
        default:
            return state;
    }
};