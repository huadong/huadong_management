import React, {Component} from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import getContent from '../../../utils/content';
import Editor from '../../../share-component/eidtor/addEditor';
import { Button,Card, Col, Row,Form,Input,Radio,Switch,DatePicker} from 'antd';
import config from "../../../config";
import msg from '../../../utils/msg';
import {withRouter} from 'react-router-dom';
import {getMap,getVal} from '../../../utils/map';
import {getStorage}from '../../../utils/auth';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state={
            tags:[],
            radioValue1:1,
            radioValue2:1,
        }
    }

    handleRadioChange1 =(e) =>{
        e.preventDefault();
        this.setState({
            radioValue1: e.target.value,
        });
    };

    handleRadioChange2 =(e) =>{
        e.preventDefault();
        this.setState({
            radioValue2: e.target.value,
        });
    };

    // handleChangeTags = (values)=>{
    //     console.log("index",values);
    //     this.setState({
    //         tag: values,
    //     });
    // };

    handleSearch = (e) => {
        e.preventDefault();
        const arr = ["advertOpen","recommendOpen","reliefOpen","rewardOpen","spreadOpen"]
        let query ={};
        this.props.form.validateFields((err, values) => {
            console.log('Received values of form: ', values);
            for(let i in values){
                if(i==="fixedReleaseTime"&&!!values[i]){
                    query.fixedReleaseTime=moment(moment(values[i]).format(config.DAY)).format(config.TIME);
                }else if(i==="sourceReleaseTime"&&!!values[i]){
                    query.sourceReleaseTime=moment(moment(values[i]).format(config.DAY)).format(config.TIME);
                }else if(arr.indexOf(i)!==-1){
                    console.log("ok");
                    query[i]=!!values[i]?1:0
                }else{
                    if(!!values[i]){
                        query[i]=values[i];
                    }
                }

            }
            if(query.hasOwnProperty('date')){
                delete query.date;
            }
            query.content=this.state.content;
            console.error(query);
            this.props.addArticle(query);
            // this.props.fetchData(1,query)

        });

        // this.setState({
        //     query:query
        // })
        // const query =this.props.queryData;
        // query();
    };

    handleDeleteTag =(i) =>{
        const tags = this.state.tags.slice(0);
        tags.splice(i, 1);
        this.setState({ tags });
        console.log(this.state.tags)
    };

    handleAddition =(tag)=> {
        const tags = [].concat(this.state.tags, tag);
        this.setState({ tags });
        console.log(this.state.tags);
    };

    handleGetContent = (data) => {
        msg(config.SUCCESS,"保存成功");
        this.setState({
            content:data
        })
    };
    render() {
        const {adding,addArticle,releasing,release} =this.props;
        const map = getStorage('map');
        console.log("map",map);
        let channelMap = getMap(map.channel);
        let sourceMap = getMap(map.sourceType);
        let attrMap = getMap(map.attr);
        let releaseMap = getMap(map.releaseState);
        let commentMap = getMap(map.commentState);

        channelMap.unshift(config.AddALL);
        sourceMap.unshift(config.AddALL);
        attrMap.unshift(config.AddALL);
        releaseMap.unshift(config.AddALL);
        commentMap.unshift(config.AddALL);
        console.log(release);

        const {getFieldDecorator}=this.props.form;
        return (
            <div>
                {getContent(
                <div className="content">
                    <Editor onChange={(values)=>this.handleChangeTags(values)}
                            getHtml={(data)=>this.handleGetContent(data)}
                            getField={getFieldDecorator}
                            channelMap={channelMap}
                            sourceMap={sourceMap}
                    />
                </div>
            )}
                <br/>
                <div>
                    <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
                        <Row gutter={16}>
                            <Col span={12}>
                                {
                                    getContent(
                                        <Card  title="其他" className="content">
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="文章质量" style={{marginBottom:"30px"}}>
                                                        {getFieldDecorator('articleQuality',{initialValue:0})(
                                                            <RadioGroup>
                                                                <RadioButton value={0}>常规</RadioButton>
                                                                <RadioButton value={1}>高质量</RadioButton>
                                                            </RadioGroup>
                                                        )}

                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={20}>
                                                    <FormItem label="文章时效" style={{marginBottom:"30px"}}>
                                                        {getFieldDecorator('timeEffect',{initialValue:"1"})(
                                                            <RadioGroup>
                                                                <RadioButton value="1">无时效</RadioButton>
                                                                <RadioButton value="2">1天</RadioButton>
                                                                <RadioButton value="3">3天</RadioButton>
                                                            </RadioGroup>
                                                        )}

                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="地区属性" style={{marginBottom:"29px"}}>
                                                        {getFieldDecorator('areaAttribute')(
                                                            <Input placeholder="添加地区属性" disabled/>
                                                        )}

                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="默认浏览" style={{marginBottom:"28px"}}>
                                                        {getFieldDecorator('viewNum',{initialValue:"0"})(
                                                            <Input placeholder="默认浏览数"/>
                                                        )}

                                                    </FormItem>
                                                </Col>
                                            </Row>

                                        </Card>
                                    )
                                }
                            </Col>
                            <Col span={12}>
                                {
                                    getContent(
                                        <Card  title="控制属性(OFF：关闭；ON：开启)" className="content special">
                                            <FormItem label="是否送金币" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('rewardOpen')(
                                                    <Switch />
                                                )}

                                            </FormItem>
                                            <FormItem label="免责声明" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('reliefOpen')(
                                                    <Switch  />
                                                )}

                                            </FormItem>
                                            <FormItem label="是否展开全文" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('spreadOpen')(
                                                    <Switch  />
                                                )}

                                            </FormItem>
                                            <FormItem label="相关推荐" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('recommendOpen')(
                                                    <Switch  />
                                                )}

                                            </FormItem>
                                            <FormItem label="关闭广告" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('advertOpen')(
                                                    <Switch  />
                                                )}

                                            </FormItem>
                                            <FormItem label="评论状态" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('commentState',{initialValue:1})(
                                                    <RadioGroup defaultValue={1}>
                                                        <Radio value={1}>正常评论</Radio>
                                                        <Radio value={2}>必审评论</Radio>
                                                        <Radio value={3}>隐藏评论</Radio>
                                                        <Radio value={4}>关闭评论</Radio>
                                                    </RadioGroup>
                                                )}
                                            </FormItem>
                                            <FormItem label="系统定向" style={{marginBottom:"0px"}}>
                                                {getFieldDecorator('directionalPush',{initialValue:1})(
                                                    <RadioGroup defaultValue={1}>
                                                        <Radio value={1}>无</Radio>
                                                        <Radio value={2}>Android</Radio>
                                                        <Radio value={3}>IOS</Radio>
                                                    </RadioGroup>
                                                )}

                                            </FormItem>
                                        </Card>
                                    )
                                }
                            </Col>
                        </Row>
                        <br/>
                        <Row gutter={16}>
                            <Col span={24}>
                                {getContent(
                                    <Card title="提交">
                                        <Row gutter={16}>
                                            <Col>
                                                <FormItem label="延迟发布时间">
                                                    {getFieldDecorator('fixedReleaseTime')(
                                                        <DatePicker/>
                                                    )}
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <Row gutter={16}>
                                            <Col>
                                                <Button type="default" htmlType="submit" style={{width:"100px",marginRight:"20px"}}>提交</Button>
                                                <Button type="primary" htmlType="submit" style={{width:"100px"}}>提交并发布</Button>
                                            </Col>
                                        </Row>
                                    </Card>
                                )}
                            </Col>
                        </Row>
                    </Form>

                </div>

            </div>

        )
    }
}

AppComponent.propTypes = {
    adding:PropTypes.bool.isRequired,
    releasing:PropTypes.bool.isRequired,
    addArticle:PropTypes.func.isRequired,
    release:PropTypes.func.isRequired,
};

const WrappedAdvancedAppComponent = withRouter(Form.create()(AppComponent));


export default WrappedAdvancedAppComponent;
