import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    ADD_REQUEST: null,
    ADD_SUCCESS: null,
    ADD_FAILURE: null,

    FETCHROLE_REQUEST: null,
    FETCHROLE_SUCCESS: null,
    FETCHROLE_FAILURE: null,

    FETCHPERROLE_REQUEST: null,
    FETCHPERROLE_SUCCESS: null,
    FETCHPERROLE_FAILURE: null,

    DELETE_REQUEST: null,
    DELETE_SUCCESS: null,
    DELETE_FAILURE: null,
})