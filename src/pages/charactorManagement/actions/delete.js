import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';

function deleting() {
    return {
        type: actionTypes.DELETE_REQUEST
    };
}

function deleted(data) {
    return {
        type: actionTypes.DELETE_SUCCESS,
        data,
    };
}

function deleteFail(error) {
    return {
        type: actionTypes.DELETE_FAILURE,
        error,
    };
}

export default function addData(params, action) {

    return (dispatch) => {
        dispatch(deleting());
        const token = getToken();
        const url = action == "add" ? config.PersonalCenter.addRole : config.PersonalCenter.modifyPerRo + params.roleCode;
        const rq = action == "add" ? req.postRequestDetail : req.putRequestDetail;
        if (action == "modify") {
            delete params["roleCode"];
        }
        req.deleteRequestDetail(token, config.PersonalCenter.deleteRole + params.roleCode).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.retCode.message) {
                    dispatch(deleted(responseJson.retCode.message));
                } else {
                    dispatch(deleted({}))
                }
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(deleteFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
            else {
                dispatch(deleteFail(responseJson.retCode.message));
            }
        }).catch((err) => {
            dispatch(deleteFail(err));
            console.error(err);
        });
    }
}