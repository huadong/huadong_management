import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function getPerRoing() {
    return {
        type: actionTypes.FETCHPERROLE_REQUEST
    };
}

function getPerRoed(data) {
    return {
        type: actionTypes.FETCHPERROLE_SUCCESS,
        data,
    };
}

function getPerRoFail(error) {
    return {
        type: actionTypes.FETCHPERROLE_FAILURE,
        error,
    };
}

export default function getInfo(params) {
    return (dispatch) => {
        dispatch(getPerRoing());
        const token = getToken();
        req.getRequestDetail(token, config.PersonalCenter.getPersonalRole + "code=" + params).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                (responseJson.data) ? (
                    dispatch(getPerRoed(responseJson.data))
                ) : (
                    dispatch(getPerRoed({}))
                )
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(getPerRoFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(getPerRoFail(err));
            console.error(err);
        });
    }

}