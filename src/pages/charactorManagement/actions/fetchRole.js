import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';

function fetchRoling() {
    return {
        type: actionTypes.FETCHROLE_REQUEST,
    };
}

function fetchRoled(data) {
    return {
        type: actionTypes.FETCHROLE_SUCCESS,
        data,
    };
}

function fetchRoleFail(error) {
    return {
        type: actionTypes.FETCHROLE_FAILURE,
        error,
    };
}

export default function fetchData(params) {

    return (dispatch) => {
        dispatch(fetchRoling());
        const token = getToken();
        console.log(config.Sensitive.querySensitiveWords);
        //要改
        req.getRequestDetail(token, config.PersonalCenter.getRoleTree).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data && responseJson.data.length !== 0) {

                    dispatch(fetchRoled(responseJson.data));
                } else {
                    dispatch(fetchRoled({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(fetchRoleFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(fetchRoleFail(err));
            console.error(err);
        });
    }
}