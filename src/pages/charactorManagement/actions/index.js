import fetchData from './fetch';
import addData from './add';
import fetchRole from './fetchRole';
import fetchPerRo from './fetchPersonalRole';
import deleteRole from './delete';

export {
    fetchData,
    addData,
    fetchRole,
    fetchPerRo,
    deleteRole,
}