import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

class Container extends Component {
    componentDidMount(){

    }

    render() {
        return(
            <ViewComponent {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roleInfo: state.charMana.roleInfo,
        fetching: state.charMana.fetching,
        roles: state.charMana.roles,
        addMessage: state.charMana.addMessage,
        action: state.charMana.action,
        perRoleInfo: state.charMana.perRoleInfo,
        deleteMessage: state.charMana.deleteMessage,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params))
        },
        fetchRole: () => {
            dispatch(actions.fetchRole())
        },
        addData: (params, action) => {
            dispatch(actions.addData(params, action))
        },
        fetchPerRo: (params) => {
            dispatch(actions.fetchPerRo(params))
        },
        deleteRole: (params) => {
            dispatch(actions.deleteRole(params))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);