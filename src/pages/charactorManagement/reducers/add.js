import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.ADD_REQUEST:
            return {
                ...state,
                fetched: false,
            }
        case actionTypes.ADD_SUCCESS:
            return {
                ...state,
                fetched: true,
                addMessage: action.data,
                action: action.action,
            }
        case actionTypes.ADD_FAILURE:
            return {
                ...state,
                fetched: false,
                addMessage: action.error,
            }
        default:
            return state;
    }

}