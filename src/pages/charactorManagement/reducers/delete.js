import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.DELETE_REQUEST:
            return {
                ...state,
            }
        case actionTypes.DELETE_SUCCESS:
            return {
                ...state,
                deleteMessage: action.data,
            }
        case actionTypes.DELETE_FAILURE:
            return {
                ...state,
                deleteMessage: action.error,
            }
        default:
            return state;
    }

}