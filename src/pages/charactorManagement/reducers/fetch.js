import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCH_REQUEST:
            return {
                ...state,
                fetched: false,
            }
        case actionTypes.FETCH_SUCCESS:
            return {
                ...state,
                fetched: true,
                roleInfo: action.data,
                addMessage: "",
                action: "",
                deleteMessage: "",
            }
        case actionTypes.FETCH_FAILURE:
            return {
                ...state,
                fetched: false,
            }
        default:
            return state;
    }

}