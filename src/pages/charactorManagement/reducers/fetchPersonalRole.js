import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCHPERROLE_REQUEST:
            return {
                ...state,
            }
        case actionTypes.FETCHPERROLE_SUCCESS:
            return {
                ...state,
                perRoleInfo: action.data,
            }
        case actionTypes.GETINFO_FAILURE:
            return {
                ...state,
            }
        default:
            return state;
    }

}