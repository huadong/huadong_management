import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCHROLE_REQUEST:
            return {
                ...state,
            }
        case actionTypes.FETCHROLE_SUCCESS:
            return {
                ...state,
                roles: action.data,
            }
        case actionTypes.FETCHROLE_FAILURE:
            return {
                ...state,
            }
        default:
            return state;
    }

}