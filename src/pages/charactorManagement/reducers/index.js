import fetchData from './fetch';
import addData from './add';
import fetchRole from './fetchRole';
import fetchPerRo from './fetchPersonalRole';
import deleteRole from './delete';
import reduceReducers from 'reduce-reducers';


const initialState = {
    roleInfo: {},
    fetching: false,
    addMessage: "",
    deleteMessage: "",
    modifyMessage: "",
    roles: {},
    perRoleInfo: {},
    deleteMessage: ""
}


const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => addData(state, action),
    (state = initialState, action) => fetchRole(state, action),
    (state = initialState, action) => fetchPerRo(state, action),
    (state = initialState, action) => deleteRole(state, action),
)

export default reducer;