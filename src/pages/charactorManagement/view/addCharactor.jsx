import React from 'react';
import {Form, Input, Checkbox, Row, Table} from 'antd';
import './label.css';

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const TextArea = Input.TextArea;

const formItemLayout = {
    labelCol: {span: 4},
    wrapperCol: {span: 20}
}

const contentOptions = [
    {label: '线上管理内容', value: 'online'},
    {label: '网络文章初审', value: 'firstCheck'},
    {label: '网络文章复审', value: 'repeatCheck'},
];
const sensitiveOptions = [
    {label: '敏感词管理', value: 'sensitive'},
];
const platOptions = [
    {label: '审核记录查询', value: 'checkRecord'},
    {label: '审核成员管理', value: 'checkMember'},
]

class AddCharactor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        this.props.fetchRole();
        console.log(this.props);
        if (this.props.roleCode)
            this.props.fetchPerRo(this.props.roleCode);
    }

    handleRecursion = (params, par) => {
        params.map((item, i) => {
            par[i] = {};
            par[i].child = [];
            if (item.child.length != 0) {
                this.handleRecursion(item.child, par[i].child);
            }
            if (item.child.length == 0)
                delete par[i].child;
            par[i].value = item.id;
            par[i].label = item.menuName;
        })
    }

    componentWillReceiveProps(nextProps) {
        const array = [];
        if (nextProps.roles && JSON.stringify(nextProps.roles) != '{}') {
            this.handleRecursion(nextProps.roles, array);
            this.setState({
                data: array,
            })
        }
    }

    handleChangeCheckBox = (value) => {
        for (let i = 0; i < 5; i++) {
            const data = {};
            data[`menuIds${i}`] = value;
            this.props.form.setFieldsValue(data);
        }
        console.log(value);
    }

    render() {

        console.log(this.state.data)

        const {getFieldDecorator} = this.props.form;

        console.log(this.props.form.getFieldsValue());

        if (this.props.handleInfo)
            this.props.handleInfo(this.props.form.getFieldsValue());

        return (
            <div>
                <Form>
                    <Row>
                        <FormItem label="角色编码"
                                  {...formItemLayout}
                        >
                            {
                                getFieldDecorator("roleCode", {
                                    initialValue: this.props.perRoleInfo ? this.props.perRoleInfo.roleCode : ""
                                })(
                                    <Input/>)}
                        </FormItem>
                    </Row>
                    <Row>
                        <FormItem label="角色名称"
                                  {...formItemLayout}
                        >
                            {
                                getFieldDecorator("roleName", {
                                    initialValue: this.props.perRoleInfo ? this.props.perRoleInfo.roleName : ""
                                })(
                                    <Input/>)}
                        </FormItem>
                    </Row>
                    <Row>
                        <FormItem label="角色描述"
                                  {...formItemLayout}
                        >
                            {
                                getFieldDecorator("roleDesc", {
                                    initialValue: this.props.perRoleInfo ? this.props.perRoleInfo.roleDesc : ""
                                })(
                                    <TextArea autosize={{minRows: 3, maxRows: 3}}/>)}
                        </FormItem>
                    </Row>
                    <Row>
                        <FormItem label="设置权限"
                                  {...formItemLayout}
                        >
                            {
                                this.state.data.map((item, i) => {
                                    return (
                                        <div key={i}>
                                            <span className="roleName">{item.label}</span><br/>
                                            {getFieldDecorator(`menuIds${i}`, {
                                                initialValue: this.props.perRoleInfo ? this.props.perRoleInfo.menuIds : []
                                            })(
                                                <CheckboxGroup options={item.child}
                                                               onChange={this.handleChangeCheckBox}/>)}
                                        </div>
                                    )
                                })
                            }
                        </FormItem>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default Form.create()(AddCharactor);