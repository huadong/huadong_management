import React from 'react';
import MultiButton from '../../../share-component/multi-button';
import {Table, Modal} from 'antd';
import AddCharactor from './addCharactor';

let params;

export default class CharactorManagement extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            roleInfo: {},
            pagination: {
                count: 1
            },
            loading: false,
        }
    }

    componentDidMount() {
        this.props.fetchData({});
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        const {count} = this.state.pagination;
        if (nextProps) {
            this.setState({
                roleInfo: nextProps.roleInfo,
                pagination: {
                    pageSize: nextProps.roleInfo.pageSize,
                    total: nextProps.roleInfo.totalCount,
                    count: count,
                },
                loading: true,
            }, () => {
                this.setState({
                    loading: false
                })
            })
            if (nextProps.addMessage && nextProps.addMessage != "") {
                if (nextProps.addMessage != "OK") {
                    Modal.error({
                        title: nextProps.addMessage,
                    })
                }
                else {
                    const message = nextProps.action == "add" ? "添加成功" : "修改成功";
                    Modal.success({
                        title: message
                    })
                }
                this.props.fetchData({
                    page: this.state.pagination.count,
                });
            }
            if (nextProps.deleteMessage && nextProps.deleteMessage != "") {
                if (nextProps.deleteMessage != "OK") {
                    Modal.error({
                        title: nextProps.deleteMessage,
                    })
                }
                else {
                    Modal.success({
                        title: "删除成功"
                    })
                }
                this.props.fetchData({
                    page: this.state.pagination.count,
                });
            }
        }
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        this.props.fetchData({
            page: pagination.current,
        });
    }

    handleOk = () => {
        const params = this.refs.addChar.getFieldsValue();
        let menuIds = [];
        for (let o in params) {
            if (!params[o])
                delete params[o]
        }
        for (let i = 0; i < 5; i++) {
            if (params[`menuIds${i}`]) {
                menuIds = menuIds.concat(params[`menuIds${i}`]);
                delete params[`menuIds${i}`];
            }
        }
        console.log(params);
        params.menuIds = [...new Set(menuIds)];
        params.weight = 0;
        console.log(params);
        this.props.addData(params, "add");
    }

    handleInfo = (param) => {
        let menuIds = [];
        for (let o in param) {
            if (!param[o]) {
                delete param[o]
            }
        }
        for (let i = 0; i < 5; i++) {
            if (param[`menuIds${i}`]) {
                menuIds = menuIds.concat(param[`menuIds${i}`]);
                delete param[`menuIds${i}`];
            }
        }
        param.menuIds = [...new Set(menuIds)];
        params = param;
    }

    render() {
        console.log(this.props);

        const columns = [{
            title: '角色名称',
            dataIndex: 'roleName',
            width: "200px"
        }, {
            title: '角色描述',
            dataIndex: 'roleDesc',
            width: "200px"
        }, {
            title: '关联权限',
            dataIndex: 'menuNames',
            width: "500px",
            render: props => {
                return props.join('/');
            }
        }, {
            title: '操作',
            render: (props, row) => {
                return (
                    <div>
                        <MultiButton
                            title="修改"
                            type="primary"
                            modalTitle="修改角色"
                            modalText="修改角色"
                            select={true}
                            handleOk={() => {
                                this.props.addData({...params, weight: row.weight}, "modify")
                            }}
                        >
                            <AddCharactor fetchRole={this.props.fetchRole}
                                          roles={this.props.roles}
                                          fetchPerRo={this.props.fetchPerRo}
                                          perRoleInfo={this.props.perRoleInfo}
                                          roleCode={row.roleCode}
                                          handleInfo={this.handleInfo}
                            />
                        </MultiButton>
                        &nbsp;
                        <MultiButton
                            title="删除"
                            modalTitle="删除成员"
                            modalText="确定要将这个角色删除吗？"
                            handleOk={() => {
                                this.props.deleteRole({roleCode: row.roleCode})
                            }}
                        />
                    </div>
                )
            }
        }];

        return (
            <div>
                <MultiButton center={true}
                             type="primary"
                             title="添加角色"
                             modalTitle="添加角色"
                             select={true}
                             handleOk={this.handleOk}
                >
                    <AddCharactor fetchRole={this.props.fetchRole}
                                  roles={this.props.roles}
                                  addData={this.props.addData}
                                  ref="addChar"
                    />
                </MultiButton>
                <Table columns={columns}
                       dataSource={this.state.roleInfo.records}
                       rowKey="id"
                       loading={this.state.loading}
                       onChange={this.handleTableChange}
                       pagination={this.state.pagination}
                />
            </div>
        )
    }
}