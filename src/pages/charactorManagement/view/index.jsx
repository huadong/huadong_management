import React from 'react';
import CharactorManagement from './charactorManagement';
import getContent from '../../../utils/content';


export default class AppComponent extends React.Component {


    render() {

        return(
            getContent(
                <div className="content">
                    <CharactorManagement {...this.props}/>
                </div>)
        )
    }
}