import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    CHECK_REQUEST: null,
    CHECK_SUCCESS: null,
    CHECK_FAILURE: null,

    OUT_REQUEST: null,
    OUT_SUCCESS: null,
    OUT_FAILURE: null,
})