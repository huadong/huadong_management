/**
 * Created by Richie on 2018/4/12
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function checking() {
    return{
        type:actionTypes.CHECK_REQUEST
    };
}

function checked(data) {
    return {
        type: actionTypes.CHECK_SUCCESS,
        data,
    };
}

function checkFail(error) {
    return {
        type: actionTypes.CHECK_FAILURE,
        error,
    };
}
export default function checkData(data) {

    return (dispatch) => {
        dispatch(checking());
        // const entityData ={};
        // entityData.data="CHECK";
        // dispatch (checked(entityData))
        console.log(data);
        const token = getToken();
            req.putRequestDetail(token,config.checkedAndTurn,data).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    console.warn("responseJson", responseJson.data);
                    dispatch(checked(responseJson.data));
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    if(responseJson.retCode.code ===-1000){
                        dispatch(checked(responseJson.data))
                    }else{
                        dispatch(checkFail(responseJson.retCode.message));
                        msg(config.WARN, responseJson.retCode.message);
                    }
                }
            }).catch((err) => {
                dispatch(checkFail(err));
                console.error(err);
            });
    }

}