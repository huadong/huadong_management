import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,checking,fetching ,checkContent,outing,outData} = state.checkDetail.toJS();
    return {
        entityData,
        checking,
        fetching,
        checkContent,
        outing,
        outData
    };
};

const mapDispatchToProps = dispatch => ({
    fetchData:(id) => {
        dispatch(actions.fetchData(id))
    },
    checkData:(data) => {
        dispatch(actions.checkData(data));
    },
    out:(data)=>{
        dispatch(actions.out(data));
    }
});

class Container extends Component {

    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);