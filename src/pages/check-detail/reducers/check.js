/**
 * Created by Richie on 2018/4/12
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.CHECK_REQUEST:
            return state.merge({
                checking:false,
            });
        case actionTypes.CHECK_SUCCESS: {
            return state.merge({
                checkContent:action.data,
                checking:true,
            });
        }
        case actionTypes.CHECK_FAILURE:
            return state.merge({
                checkContent:{},
                checking:false,
            });
        default:
            return state;
    }
};