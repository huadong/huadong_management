import { fromJS } from 'immutable';
import reduceReducers from 'reduce-reducers';

import fetchData from './fetch';
import checkData from './check';
import out from './check';

const initialState = fromJS({
    entityData:{},
    checkContent:{},
    outing:false,
    outData:{},
    fetching:false,
    checking:false
});

const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => checkData(state, action),
    (state = initialState, action) => out(state, action),
);

export default reducer;