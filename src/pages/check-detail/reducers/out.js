import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.OUT_REQUEST:
            return state.merge({
                outing:false,
            });
        case actionTypes.OUT_SUCCESS: {
            return state.merge({
                outData:action.data,
                outing:true,
            });
        }
        case actionTypes.OUT_FAILURE:
            return state.merge({
                outData:{},
                outing:false,
            });
        default:
            return state;
    }
};