import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    QUERY_REQUEST: null,
    QUERY_SUCCESS: null,
    QUERY_FAILURE: null,

    OPERATE_REQUEST: null,
    OPERATE_SUCCESS: null,
    OPERATE_FAILURE: null,

    DELETE_REQUEST: null,
    DELETE_SUCCESS: null,
    DELETE_FAILURE: null,

    HOT_REQUEST: null,
    HOT_SUCCESS: null,
    HOT_FAILURE: null,
})