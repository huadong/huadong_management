/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken,setStorage } from '../../../utils/auth';
import msg from '../../../utils/msg';


function deleting() {
    return{
        type:actionTypes.DELETE_REQUEST
    };
}

function deleted(data) {
    return {
        type: actionTypes.DELETE_SUCCESS,
        data,
    };
}

function deleteFail(error) {
    return {
        type: actionTypes.DELETE_FAILURE,
        error,
    };
}
export default function deleteData(ids) {
    return (dispatch) => {
        dispatch(deleting());
        const token = getToken();

            req.deleteRequestDetail(token,config.multipleDelete,ids).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    dispatch (deleted(responseJson.data));

                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(deleteFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(deleteFail(err));
                console.error(err);
            });
    }

}