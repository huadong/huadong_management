import actionTypes from '../actionTypes';
import { replace } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken,setStorage } from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return{
        type:actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}
export default function fetchData(page,pageSize,query) {

    return (dispatch) => {
        dispatch(fetching());

        query.page=page;
        query.pageSize=pageSize;
        const token = getToken();
            req.postRequestDetail(token,config.getContent,query).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    if(!!responseJson.data.records) {
                        const arr = responseJson.data.records;
                        arr.forEach((v,k)=>{
                            !!v.releaseTime?v.releaseTime=moment(v.releaseTime).format(config.TIME):"";
                            !!v.cover?v.cover=v.cover.split(',').slice(0, 1):""
                        });
                    }

                    req.getRequestDetail(token,config.getContentMap).then((res) => {
                        let entityData ={};
                        entityData.data = responseJson.data;
                        if (res.retCode.code === 200) {
                            entityData.map=res.data;
                            console.log(entityData);
                            localStorage.removeItem("map");
                            setStorage("map",res.data);
                            dispatch (fetched(entityData));
                        }
                        //业务逻辑错误
                        if (responseJson.retCode.code < 0) {
                            dispatch(fetchFail(responseJson.retCode.message));
                            msg(config.WARN, responseJson.retCode.message);
                        }
                    }).catch((err) => {
                        dispatch(fetchFail(err));
                        console.error(err);
                    });
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(fetchFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(fetchFail(err));
                console.error(err);
            });



    }

}