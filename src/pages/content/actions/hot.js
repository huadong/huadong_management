/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken,setStorage } from '../../../utils/auth';
import msg from '../../../utils/msg';


function hotting() {
    return{
        type:actionTypes.HOT_REQUEST
    };
}

function hotted(data) {
    return {
        type: actionTypes.HOT_SUCCESS,
        data,
    };
}

function hotFail(error) {
    return {
        type: actionTypes.HOT_FAILURE,
        error,
    };
}
export default function hot(ids) {

    return (dispatch) => {
        dispatch(hotting());
        const token = getToken();

            req.putRequestDetail(token,config.multipleHot,ids).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    dispatch (hotted(responseJson.data));

                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(hotFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(hotFail(err));
                console.error(err);
            });

    }

}