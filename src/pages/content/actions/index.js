import fetchData from './fetch';
import queryData from './query';
import operate from './operate';
import deleteData from './delete';
import hot from './hot';
export {
    queryData,
    fetchData,
    operate,
    deleteData,
    hot
};