/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken,setStorage } from '../../../utils/auth';
import msg from '../../../utils/msg';


function operating() {
    return{
        type:actionTypes.OPERATE_REQUEST
    };
}

function operated(data) {
    return {
        type: actionTypes.OPERATE_SUCCESS,
        data,
    };
}

function operateFail(error) {
    return {
        type: actionTypes.OPERATE_FAILURE,
        error,
    };
}
export default function operate(ids,type,val) {

    return (dispatch) => {
        dispatch(operating());

        let path;
        switch (type){
            case 1:
                path = config.multipleModifyChannel+"?channel="+val;
                break;
            case 2:
                path = config.multipleModifyComment+"?commentState="+val;
                break;
            case 3:
                path = config.multipleModifySwitch+"?state="+val+"&type=share";
                break;
            case 4:
                path = config.multipleModifySwitch+"?state="+val+"&type=advert";
                break;
            default:
                console.log("函数使用有误!");
        }
        const token = getToken();
        console.warn(path,ids,token);

            req.putRequestDetail(token,path,ids).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    dispatch (operated(responseJson.data));

                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(operateFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(operateFail(err));
                console.error(err);
            });

    }

}