import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function querying() {
    return{
        type:actionTypes.QUERY_REQUEST
    };
}

function queried(data) {
    return {
        type: actionTypes.QUERY_SUCCESS,
        data,
    };
}

function queryFail(error) {
    return {
        type: actionTypes.QUERY_FAILURE,
        error,
    };
}

export default function queryData() {

    return (dispatch) => {
        dispatch(querying());
        const queryContent={};
        dispatch(queried(queryContent))
        // const token = getToken();
        //
        //
        //     req.postRequestDetailDetail(req.tokenHeader(token), config.queryContent).then((responseJson) => {
        //         if (responseJson.retCode.code === 200) {
        //             //用于格式化时间
        //             const moment = require('moment');
        //             console.warn("responseJson", responseJson.data);
        //             (responseJson.data && responseJson.data.length !==0)?(
        //                 dispatch(queried())
        //             ):(
        //                 dispatch(queried({}))
        //             )
        //         }
        //         //业务逻辑错误
        //         if (responseJson.retCode.code < 0) {
        //             dispatch(queryFail(responseJson.retCode.message));
        //             msg(config.WARN, responseJson.retCode.message);
        //         }
        //     }).catch((err) => {
        //         dispatch(queryFail(err));
        //         console.error(err);
        //     });
        // } else {
        //     dispatch(queryFail("-2"));
        //     msg(config.WARN, "登录已过期，请重新登录");
        //     setTimeout(dispatch(routerActions.push("/login")), 500);
        // }

    }

}