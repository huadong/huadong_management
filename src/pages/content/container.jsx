import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,queryContent,fetching,querying,operateData,operating,deleteContent, deleting,hotting,hotData } = state.content.toJS();
    return { entityData,queryContent,fetching,querying,operateData,operating,deleteContent, deleting,hotting,hotData };
};

const mapDispatchToProps = dispatch => ({
    fetchData:(page,pageSize,query) => {
        dispatch(actions.fetchData(page,pageSize,query))
    },
    queryData:(date) => {
        dispatch(actions.queryData(date));
    },
    operate:(ids,type,val)=>{
        dispatch(actions.operate(ids,type,val))
    },
    deleteData:(ids)=>{
        dispatch(actions.deleteData(ids))
    },
    hot:(ids)=>{
        dispatch(actions.hot(ids))
    },
});

class Container extends Component {
    componentDidMount() {
        this.props.fetchData(1,10,{});
    }

    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}

Container.propTypes = {
    fetchData:PropTypes.func.isRequired,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);