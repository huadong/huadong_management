/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.DELETE_REQUEST:
            return state.merge({
                deleting:false,
            });
        case actionTypes.DELETE_SUCCESS: {
            return state.merge({
                deleteContent:action.data,
                deleting:true,
            });
        }
        case actionTypes.DELETE_FAILURE:
            return state.merge({
                deleteContent:{},
                deleting:false,
            });
        default:
            return state;
    }
};