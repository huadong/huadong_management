/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.HOT_REQUEST:
            return state.merge({
                hotting:false,
            });
        case actionTypes.HOT_SUCCESS: {
            return state.merge({
                hotData:action.data,
                hotting:true,
            });
        }
        case actionTypes.HOT_FAILURE:
            return state.merge({
                hotData:{},
                hotting:false,
            });
        default:
            return state;
    }
};