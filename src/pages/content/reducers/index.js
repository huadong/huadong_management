import { fromJS } from 'immutable';
import reduceReducers from 'reduce-reducers';

import fetchData from './fetch';
import queryData from './query';
import operate from './operate';
import deleteData from './delete';
import hot from './hot';

const initialState = fromJS({
    entityData:{},
    queryContent:{},
    deleteContent:{},
    operateData:{},
    hotData:{},

    fetching:false,
    querying:false,
    operating:false,
    deleting:false,
    hotting:false,
});

const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => queryData(state, action),
    (state = initialState, action) => operate(state, action),
    (state = initialState, action) => deleteData(state, action),
    (state = initialState, action) => hot(state, action),
);

export default reducer;