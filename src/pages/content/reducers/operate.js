/**
 * Created by Richie on 2018/4/19
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.OPERATE_REQUEST:
            return state.merge({
                operating:false,
            });
        case actionTypes.OPERATE_SUCCESS: {
            return state.merge({
                operateData:action.data,
                operating:true,
            });
        }
        case actionTypes.OPERATE_FAILURE:
            return state.merge({
                operateData:{},
                operating:false,
            });
        default:
            return state;
    }
};