import React , { Component } from 'react';
import PropTypes from 'prop-types';
import {Spin} from 'antd';
import SearchForm from './search-form';
import getContent from '../../../utils/content';

class AppComponent extends Component {
    render() {
        const {entityData,queryContent,fetching,querying,queryData,fetchData,operateData,operating,operate,hotData,deleteContent,deleteData,hotting,deleting,hot} = this.props;
        if(!entityData.data){
            return (
                <Spin size="large" />
            )
        }else {
        }
        return (
            getContent(
                <div className="content">
                    <SearchForm
                        fetchData={fetchData}
                        operate={operate}
                        queryData={queryData}
                        hotData={hotData}
                        deleteContent={deleteContent}

                        entityData={entityData}
                        operateData={operateData}
                        queryContent={queryContent}
                        hot={hot}
                        deleteData={deleteData}

                        hotting={hotting}
                        deleting={deleting}
                        fetching={fetching}
                        querying={querying}
                        operating={operating}


                    />
                </div>
            )
        );

    }
}

AppComponent.propTypes = {
    entityData:PropTypes.object.isRequired,
    queryContent:PropTypes.object.isRequired,
    deleteContent:PropTypes.object.isRequired,
    hotData:PropTypes.object.isRequired,
    operateData:PropTypes.object.isRequired,
    fetching:PropTypes.bool.isRequired,
    querying:PropTypes.bool.isRequired,
    hotting:PropTypes.bool.isRequired,
    deleting:PropTypes.bool.isRequired,
    operating:PropTypes.bool.isRequired,
    fetchData:PropTypes.func.isRequired,
    operate:PropTypes.func.isRequired,
    deleteData:PropTypes.func.isRequired,
    hot:PropTypes.func.isRequired,
    queryData:PropTypes.func.isRequired
};

export default AppComponent;
