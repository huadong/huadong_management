import React,{Component} from 'react';
import moment from 'moment';
import {Link} from 'react-router-dom';

import { Form, Row, Col, Input, Button ,DatePicker,Table} from 'antd';
import ConfirmModal from '../../../share-component/modal';
import SelectModal from '../../../share-component/select-modal';
import Selection from '../../../share-component/select';
import {withRouter} from 'react-router-dom';
import {getMap,getVal} from '../../../utils/map';
import {getStorage}from '../../../utils/auth';
import config from '../../../config';
import msg from '../../../utils/msg';



const FormItem = Form.Item;

const { RangePicker }=DatePicker;

class SearchForm extends Component {
    constructor(props){
        super(props);
        this.state= {
            sourceType:"",
            channel:"",
            attribute:"",
            status:"",
            query:{},
            selectedRowKeys:[],
            selectMalVal:"",
            page:1,
            pageSize:10,
            expand: false,
            queryList:false,


            //defaultMultiple
            multipleChannel:"1",
            multipleComment:"1",
            multipleShare:0,
            multipleAdvert:0,
        };
    }

    // shouldComponentUpdate(nextProps,nextState){
    //     return this.state.page!==nextState.page ||this.state.query!==nextState.query ||this.props.operating!==nextProps.operating&&nextProps.operating ||this.props.deleting!==nextProps.deleting&&nextProps.deleting ||this.props.hotting!==nextProps.hotting&&nextProps.hotting
    // }

    componentWillReceiveProps(nextProps,nextState){
        //切换页面清空选择
        if(this.state.page!==nextState.page){
            this.setState({
                selectedRowKeys:[]
            });

        }
    }

    componentWillUpdate(nextProps,nextState){
        //切换page回调
        if(this.state.page!==nextState.page){
            this.props.fetchData(nextState.page,nextState.pageSize,{})
        }
        if(this.state.pageSize!==nextState.pageSize){
            this.props.fetchData(nextState.page,nextState.pageSize,{})
        }
        //查询成功回调
        if(this.state.query!==nextState.query){
            const {query} = nextState;
            this.props.fetchData(nextState.page,nextState.pageSize,query)
        }
        //operate成功回调
        if(this.props.operating!==nextProps.operating&&nextProps.operating){
            msg(config.SUCCESS,nextProps.operateData.success+"条数据修改成功");
            this.props.fetchData(nextState.page,nextState.pageSize,{});

        }
        //delete成功回调
        if(this.props.deleting!==nextProps.deleting&&nextProps.deleting){
            msg(config.SUCCESS,nextProps.deleteContent.success+"条数据删除成功");
            console.log("11111111")
            this.props.fetchData(nextState.page,nextState.pageSize,{});

        }
        //热推成功回调
        if(this.props.hotting!==nextProps.hotting&&nextProps.hotting){
            msg(config.SUCCESS,nextProps.hotData.success+"条数据热推成功");
            this.props.fetchData(nextState.page,nextState.page,{});

        }

    }

    //切换页面
    handleChangePages = (page) =>{
        this.setState({
            page:page
        });
    };


    //查询
    handleSearch = (e) => {
        e.preventDefault();
        let query ={};
        this.props.form.validateFields((err, values) => {
            console.log('Received values of form: ', values);
            for(let i in values){
                if(i==="date"&&!!values[i]&&values[i].length===2){
                    query.startTime=moment(moment(values[i][0]).format(config.DAY)).format(config.TIME);
                    query.endTime=moment(moment(values[i][1]).format(config.DAY)).format(config.TIME);
                }else {
                    if(!!values[i]){
                        query[i]=values[i];
                    }
                }

            }
            if(query.hasOwnProperty('date')){
                delete query.date;
            }
            console.error(query);
            this.props.fetchData(1,this.state.pageSize,query)

        });
        console.log(query);
        this.setState({
            query:query
        })
        // const query =this.props.queryData;
        // query();
    };


    //批量设置selectModal中所选中值
    handleSelectMalChange=(values)=>{
        this.setState({
            selectMalVal:values
        })
    };

    //批量删除
    handleMultipleDelete = () =>{
        this.props.deleteData(this.state.selectedRowKeys)
    };

    //批量热推
    handleMultipleHot = () =>{
        this.props.hot(this.state.selectedRowKeys)
    };

    //热推
    handleHot = (id) => {
        let arr = [];
        arr[0] = id;
        this.props.hot(arr);
    };

    //热推
    handleDelete = (id) => {
        let arr = [];
        arr[0] = id;
        this.props.deleteData(arr);
    };

    //批量操作
    handleMultipleSelect = (type) => {
            let choose;
        switch (type){
            case 1:
                choose = this.state.multipleChannel;
                break;
            case 2:
                choose = this.state.multipleComment;
                break;
            case 3:
                choose = this.state.multipleShare;
                break;
            case 4:
                choose = this.state.multipleAdvert;
                break;
            default:
                console.log("函数使用有误!");
        }
        const val = this.state.selectMalVal===""?choose:this.state.selectMalVal;
        console.error(this.state.selectedRowKeys,type,val);
        this.props.operate(this.state.selectedRowKeys,type,val);
    };

    handleTableOnChange= (selectedRowKeys) => {
        this.setState({selectedRowKeys});
    };

    handleChangePaginationSize = (current,pageSize) => {
        console.warn("size",current,pageSize);
        this.setState({
            pageSize:pageSize
        })
    }

    render() {
        const {entityData} = this.props;
        const {selectedRowKeys}=this.state;
        const map = getStorage('map');
        console.error(map.releaseState);
        let channelMap = getMap(map.channel);
        let sourceMap = getMap(map.sourceType);
        let attrMap = getMap(map.attr);
        let releaseMap = getMap(map.releaseState);
        let commentMap = getMap(map.commentState);

        const data = entityData.hasOwnProperty('data')?entityData.data.records:[];
        const ppp={defaultCurrent:entityData.data.page,showSizeChanger:true,onShowSizeChange:this.handleChangePaginationSize, total:entityData.data.totalCount, onChange:this.handleChangePages};
        //定义列表
        const columns = [
            {
                title: '文章Id',
                dataIndex: 'id',
                className:'largeTable'
            }, {
                title: '应用封面',
                // dataIndex: 'cover',
                className:'largeTable',
                render: props => {
                    return(
                        !!props.cover?(<img width={60} src={props.cover[0]} alt="应用封面"/>):("")
                    )
                },

            }, {
                title: '标题',
                // dataIndex: 'title',
                className:'largeTable',
                render: props => {
                    return(
                        <span style={{"width":"60px","display":"inline-block"}}><a href={props.sourceUrl}>{props.title}</a></span>
                    )
                },
            }, {
                title: '状态',
                className:'largeTable',
                render: props => {
                    return(
                        <span>{!getVal(map.releaseState,props.releaseState)?"无":getVal(map.releaseState,props.releaseState)}</span>
                    )
                },
            }, {
                title: '频道',
                className:'largeTable',
                render: props => {
                    return(
                        <span>{!getVal(map.channel,props.channel)?"无":getVal(map.channel,props.channel)}</span>
                    )
                },
            }, {
                title: '来源',
                dataIndex: 'sourceName',
                className:'largeTable'
            }, {
                title: '浏览',
                dataIndex: 'viewNum',
                className:'largeTable'
            }, {
                title: '评论',
                dataIndex: 'commentNum',
                className:'largeTable'
            }, {
                title: '发布者',
                className:'largeTable',
                render:props => {
                    return(
                        <span>{props.releaseUser}{(props.releaseUserId!==null)?(<span>(ID:{props.releaseUserId})</span>):(<span/>)}</span>
                    )
            }
            }, {
                title: '发布时间',
                // dataIndex: 'releaseTime',
                className:'largeTable',
                render: props => {
                    return(
                        <p style={{"width":"65px"}}>{props.releaseTime}</p>
                    )
                },
            }, {
                title: '操作',
                className:'largeTable',
                render: props => {
                    return (
                        <div style={{display:"flex"}}>
                            {/*<Button size="small" style={{fontSize:'12px'}} onClick={()=>history.push("/edit-article")}>编辑</Button>&nbsp;*/}
                            <ConfirmModal type="default" disabled={props.releaseState==="deleted"} size="small" btn="热推" title="文章热推" style={{fontSize:'12px'}} content="确定将此文章进行热推吗？" onConfirm={()=>this.handleHot(props.id)}/>&nbsp;
                            <ConfirmModal type="danger"  disabled={props.releaseState==="deleted"} size="small" btn="删除" okType="danger" title="文章删除" style={{fontSize:'12px'}} content="确定将此文章进行删除吗？" onConfirm={()=>this.handleDelete(props.id)}/>
                        </div>
                    )
                }
            }
        ];
        const {getFieldDecorator}=this.props.form;
        const rowSelection = {
            selectedRowKeys,
            onChange:this.handleTableOnChange,
            getCheckboxProps:record => ({
                disabled: record.releaseState ==='deleted', // Column configuration not to be checked
                name: record.name,
            }),
        }



        return (
                <Form
                    className="ant-advanced-search-form"
                    onSubmit={this.handleSearch}
                >
                    <Row gutter={8}>
                        <Col span={4}>
                            <FormItem label="文章标题">
                                {getFieldDecorator('titleLike')(
                                    <Input placeholder="文章标题" />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={3}>
                            <FormItem label="文章Id">
                                {getFieldDecorator('articleId')(
                                    <Input placeholder="文章Id" />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="编辑人员Id">
                                {getFieldDecorator('editorId')(
                                    <Input placeholder="编辑人员Id" />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="来源名称">
                                {getFieldDecorator('sourceName')(
                                    <Input placeholder="文章来源" />
                                )}
                            </FormItem>
                        </Col>

                        <Col span={4}>
                            <FormItem label="状态">
                                {getFieldDecorator('state')(
                                    <Selection msg={releaseMap.concat(config.AddALL)} dv=""/>
                                )}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="属性">
                                {getFieldDecorator('attribute')(
                                    <Selection msg={attrMap.concat(config.AddALL)} dv=""/>
                                )}
                            </FormItem>
                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={5}>
                            <FormItem label="来源类型">
                                {getFieldDecorator('sourceType')(
                                    <Selection msg={sourceMap.concat(config.AddALL)} dv=""/>
                                )}
                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="所属频道">
                                {getFieldDecorator('channel')(
                                    <Selection msg={channelMap.concat(config.AddALL)} dv=""/>
                                )}
                            </FormItem>
                        </Col>
                        <Col span={8}>
                            <FormItem label="日期">
                                {getFieldDecorator('date')(
                                    <RangePicker/>
                                )}
                            </FormItem>
                        </Col>

                        <Col span={3} style={{ textAlign: 'center' }}>
                            <FormItem>
                                <Button type="primary" htmlType="submit"  icon="search">查询文章</Button>
                            </FormItem>
                        </Col>
                        <Col span={3}>
                            <FormItem>
                                {/*<Button type="primary" icon="plus" onClick={()=>history.push("/add-article")}>新建文章</Button>*/}
                            </FormItem>

                        </Col>
                    </Row>


                    <FormItem label="批量操作">
                        <Row type="flex" justify="start" gutter={24}>
                            <Col span={2} >
                                <SelectModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="修改频道" title="批量修改频道"  content={"将选中的"+this.state.selectedRowKeys.length+"篇文章，批量修改为："} msg={channelMap} dv="1" onConfirm={(type)=>this.handleMultipleSelect(type)} selectType={1} selectChange={(values)=>this.handleSelectMalChange(values)}/>
                            </Col>
                            <Col span={2} offset={1}>
                                <ConfirmModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="删除文章" title="批量删除文章" okType="danger" content={"确定将选中的"+this.state.selectedRowKeys.length+"篇文章都进行删除吗？"} onConfirm={()=>this.handleMultipleDelete()}/>
                            </Col>
                            <Col span={2}  offset={1}>
                                <ConfirmModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="热　　推" title="批量热推" content={"确定将选中的"+this.state.selectedRowKeys.length+"篇文章都进行热推吗？"} onConfirm={()=>this.handleMultipleHot()}/>
                            </Col>
                            <Col span={2}  offset={1}>
                                <SelectModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="评　　论" title="批量设置评论" content={"将选中的"+this.state.selectedRowKeys.length+"篇文章，批量修改为："} msg={commentMap} dv="1" onConfirm={(type)=>this.handleMultipleSelect(type)} selectType={2} selectChange={(values)=>this.handleSelectMalChange(values)}/>
                            </Col>
                            <Col span={2}  offset={1}>
                                <SelectModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="分　　享" title="批量设置分享" content={"将选中的"+this.state.selectedRowKeys.length+"篇文章，批量修改为："} msg={[{val:0,name:'关闭'},{val:1,name:"开启"}]} dv={0} onConfirm={(type)=>this.handleMultipleSelect(type)} selectType={3} selectChange={(values)=>this.handleSelectMalChange(values)}/>
                            </Col>
                            <Col span={2} offset={1}>
                                <SelectModal type="primary" disabled={this.state.selectedRowKeys.length===0} btn="广　　告" title="批量设置广告" content={"将选中的"+this.state.selectedRowKeys.length+"篇文章，批量修改为："} msg={[{val:0,name:'关闭'},{val:1,name:"开启"}]} dv={0} onConfirm={(type)=>this.handleMultipleSelect(type)} selectType={4} selectChange={(values)=>this.handleSelectMalChange(values)}/>
                            </Col>
                        </Row>
                    </FormItem>
                    {/*<Test/>*/}
                    <Table rowSelection={rowSelection}  rowKey="id" dataSource={data} pagination={ppp} columns={columns} loading={!(entityData.hasOwnProperty('data'))}>
                    </Table>
                </Form>
            );
    }
}

 const WrappedAdvancedSearchForm = withRouter(Form.create()(SearchForm));

export default WrappedAdvancedSearchForm;
