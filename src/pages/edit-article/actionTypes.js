import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    EDIT_REQUEST: null,
    EDIT_SUCCESS: null,
    EDIT_FAILURE: null,
})