/**
 * Created by Richie on 2018/4/12
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function editing() {
    return{
        type:actionTypes.EDIT_REQUEST
    };
}

function edited(data) {
    return {
        type: actionTypes.EDIT_SUCCESS,
        data,
    };
}

function editFail(error) {
    return {
        type: actionTypes.EDIT_FAILURE,
        error,
    };
}

export default function editData() {

    return (dispatch) => {
        dispatch(editing());
        const entityData ={};
        entityData.data="editData";
        dispatch (edited(entityData))
        // const token = getToken();
        // if (token) {
        //     req.getRequestDetailDetail(token,req.tokenHeader(token), config.getContent).then((responseJson) => {
        //         if (responseJson.retCode.code === 200) {
        //             //用于格式化时间
        //             const moment = require('moment');
        //             console.warn("responseJson", responseJson.data);
        //             if(responseJson.data && responseJson.data.length !==0){
        //
        //                 dispatch(edited());
        //             }else {
        //                 dispatch(edited({}))
        //             }
        //
        //         }
        //         //业务逻辑错误
        //         if (responseJson.retCode.code < 0) {
        //             dispatch(editFail(responseJson.retCode.message));
        //             msg(config.WARN, responseJson.retCode.message);
        //         }
        //     }).catch((err) => {
        //         dispatch(editFail(err));
        //         console.error(err);
        //     });
        // } else {
        //     dispatch(editFail("-2"));
        //     msg(config.WARN, "登录已过期，请重新登录");
        //     setTimeout(dispatch(routerActions.push("/login")), 500);
        // }

    }

}