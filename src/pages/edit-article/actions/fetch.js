import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return{
        type:actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}

export default function fetchData() {

    return (dispatch) => {
        dispatch(fetching());
        const entityData ={};
        entityData.data="fetchData";
        dispatch (fetched(entityData))
        // const token = getToken();
        // if (token) {
        //     req.getRequestDetailDetail(token,req.tokenHeader(token), config.getContent).then((responseJson) => {
        //         if (responseJson.retCode.code === 200) {
        //             //用于格式化时间
        //             const moment = require('moment');
        //             console.warn("responseJson", responseJson.data);
        //             if(responseJson.data && responseJson.data.length !==0){
        //
        //                 dispatch(fetched());
        //             }else {
        //                 dispatch(fetched({}))
        //             }
        //
        //         }
        //         //业务逻辑错误
        //         if (responseJson.retCode.code < 0) {
        //             dispatch(fetchFail(responseJson.retCode.message));
        //             msg(config.WARN, responseJson.retCode.message);
        //         }
        //     }).catch((err) => {
        //         dispatch(fetchFail(err));
        //         console.error(err);
        //     });
        // } else {
        //     dispatch(fetchFail("-2"));
        //     msg(config.WARN, "登录已过期，请重新登录");
        //     setTimeout(dispatch(routerActions.push("/login")), 500);
        // }

    }

}