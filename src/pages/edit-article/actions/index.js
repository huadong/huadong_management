import fetchData from './fetch';
import editArticle from './edit';
export {
    fetchData,
    editArticle,
};