import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,fetching,adding} = state.editArticle.toJS();
    return {
        entityData,
        fetching,
        adding
    };
};

const mapDispatchToProps = dispatch => ({
    fetchData:() => {
        dispatch(actions.fetchData())
    },
    editArticle:() => {
        dispatch(actions.editArticle())
    }
});

class Container extends Component {
    componentDidMount(){
        this.props.fetchData();
    }
    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}

Container.propTypes = {
    fetchData:PropTypes.func.isRequired,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);