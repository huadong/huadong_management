/**
 * Created by Richie on 2018/4/12
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.EDIT_REQUEST:
            return state.merge({
                editing:false,
            });
        case actionTypes.EDIT_SUCCESS: {
            return state.merge({
                editing:true,
            });
        }
        case actionTypes.EDIT_FAILURE:
            return state.merge({
                editing:false,
            });
        default:
            return state;
    }
};