import { fromJS } from 'immutable';
import reduceReducers from 'reduce-reducers';

import fetchData from './fetch';
import editArticle from './edit';

const initialState = fromJS({
    entityData:{},
    queryContent:{},
    fetching:false,
    querying:false
});

const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => editArticle(state, action),
);

export default reducer;