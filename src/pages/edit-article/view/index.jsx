import React, {Component} from 'react';
import PropTypes from 'prop-types';

import getContent from '../../../utils/content';
import Editor from '../../../share-component/eidtor/addEditor';
import { Button,Card, Col, Row,Form,Input,Radio,Switch,DatePicker} from 'antd';
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class AppComponent extends Component {
    constructor(props) {
        super(props)
        this.state={
            radioValue1:1,
            radioValue2:1,
        }
    }

    componentDidMount() {

    }

    handleRadioChange1 =(e) =>{
        e.preventDefault();
        this.setState({
            radioValue1: e.target.value,
        });
    };

    handleRadioChange2 =(e) =>{
        e.preventDefault();
        this.setState({
            radioValue2: e.target.value,
        });
    };

    render() {
        const {entityData,fetching,editing,editArticle} =this.props;
        console.log(entityData,fetching,editing,editArticle);
        return (

            <div>
                {getContent(
                    <div className="content">
                        <Editor/>
                    </div>
                )}
                <br/>
                <div>
                    <Form className="ant-advanced-search-form" >
                        <Row gutter={16}>
                            <Col span={12}>
                                {
                                    getContent(
                                        <Card  title="其他" className="content">
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="文章质量" style={{marginBottom:"30px"}}>
                                                        <RadioGroup  defaultValue="1">
                                                            <RadioButton value="1">常规</RadioButton>
                                                            <RadioButton value="2">高质量</RadioButton>
                                                        </RadioGroup>
                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={20}>
                                                    <FormItem label="文章时效" style={{marginBottom:"30px"}}>
                                                        <RadioGroup  defaultValue="a">
                                                            <RadioButton value="a">无时效</RadioButton>
                                                            <RadioButton value="b">1天</RadioButton>
                                                            <RadioButton value="c">3天</RadioButton>
                                                        </RadioGroup>
                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="地区属性" style={{marginBottom:"29px"}}>
                                                        <Button type="primary">添加地区属性</Button>
                                                    </FormItem>
                                                </Col>
                                            </Row>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <FormItem label="默认浏览" style={{marginBottom:"28px"}}>
                                                        <Input placeholder="默认浏览数" value="0"/>
                                                    </FormItem>
                                                </Col>
                                            </Row>

                                        </Card>
                                    )
                                }
                            </Col>
                            <Col span={12}>
                                {
                                    getContent(
                                        <Card  title="控制属性(OFF：关闭；ON：开启)" className="content special">
                                            <FormItem label="是否送金币" style={{marginBottom:"0px"}}>
                                                <Switch defaultChecked />
                                            </FormItem>
                                            <FormItem label="免责声明" style={{marginBottom:"0px"}}>
                                                <Switch  />
                                            </FormItem>
                                            <FormItem label="是否展开全文" style={{marginBottom:"0px"}}>
                                                <Switch defaultChecked />
                                            </FormItem>
                                            <FormItem label="相关推荐" style={{marginBottom:"0px"}}>
                                                <Switch  />
                                            </FormItem>
                                            <FormItem label="关闭广告" style={{marginBottom:"0px"}}>
                                                <Switch defaultChecked />
                                            </FormItem>
                                            <FormItem label="评论状态" style={{marginBottom:"0px"}}>
                                                <RadioGroup onChange={this.handleRadioChange1} value={this.state.radioValue1}>
                                                    <Radio value={1}>正常评论</Radio>
                                                    <Radio value={2}>必审评论</Radio>
                                                    <Radio value={3}>隐藏评论</Radio>
                                                    <Radio value={4}>关闭评论</Radio>
                                                </RadioGroup>
                                            </FormItem>
                                            <FormItem label="系统定向" style={{marginBottom:"0px"}}>
                                                <RadioGroup onChange={this.handleRadioChange2} value={this.state.radioValue2}>
                                                    <Radio value={1}>无</Radio>
                                                    <Radio value={2}>Android</Radio>
                                                    <Radio value={3}>IOS</Radio>
                                                </RadioGroup>
                                            </FormItem>
                                        </Card>
                                    )
                                }
                            </Col>
                        </Row>
                        <br/>
                        <Row gutter={16}>
                            <Col span={24}>
                                {getContent(
                                    <Card title="提交">
                                        <Row gutter={16}>
                                            <Col>
                                                <FormItem label="延迟发布时间">
                                                    <DatePicker/>
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <Row gutter={16}>
                                            <Col>
                                                <Button type="default" style={{width:"100px",marginRight:"20px"}}>提交</Button>
                                                <Button type="primary" style={{width:"100px",marginRight:"20px"}}>提交并发布</Button>
                                                <Button type="danger" style={{width:"100px"}}>文章下架</Button>
                                            </Col>
                                        </Row>
                                    </Card>
                                )}
                            </Col>
                        </Row>
                    </Form>

                </div>

            </div>

        )
    }
}

AppComponent.propTypes = {
    entityData:PropTypes.object.isRequired,
    fetching:PropTypes.bool.isRequired,
    editing:PropTypes.bool.isRequired,
    editArticle:PropTypes.func.isRequired
};

export default AppComponent;
