import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    DETAIL_REQUEST: null,
    DETAIL_SUCCESS: null,
    DETAIL_FAILURE: null,

    QUERY_REQUEST: null,
    QUERY_SUCCESS: null,
    QUERY_FAILURE: null,
})