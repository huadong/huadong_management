import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function detailing() {
    return{
        type:actionTypes.DETAIL_REQUEST
    };
}

function detailed(data) {
    return {
        type: actionTypes.DETAIL_SUCCESS,
        data,
    };
}

function detailFail(error) {
    return {
        type: actionTypes.DETAIL_FAILURE,
        error,
    };
}
export default function detail() {

    return (dispatch) => {
        dispatch(detailing());
        const token = getToken();

            req.getRequestDetail(token,config.getCheckDetail).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    dispatch(detailed(responseJson.data));
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(detailFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(detailFail(err));
                console.error(err);
            });

    }

}