import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return{
        type:actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}
export default function fetchData(page,pageSize,params) {

    return (dispatch) => {
        dispatch(fetching());

        const token = getToken();

            req.postRequestDetail(token,config.getCheckArticleList+"?page="+page+"&pageSize="+pageSize,params).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    console.warn("responseJson", responseJson);
                    const arr = responseJson.data.dataList;
                    arr.forEach((v)=>{
                        v.sourceReleaseTime=moment(v.sourceReleaseTime).format("YYYY/MM/DD hh:mm:ss");
                        v.createTime=moment(v.createTime).format("YYYY/MM/DD hh:mm:ss");
                        v.lastModifiedTime=moment(v.lastModifiedTime).format("YYYY/MM/DD hh:mm:ss");
                    });
                    dispatch(fetched(responseJson.data))
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(fetchFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(fetchFail(err));
                console.error(err);
            });

    }

}