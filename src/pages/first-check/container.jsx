import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,fetching,detailing,detailContent,queryContent,querying } = state.firstCheck.toJS();
    return { entityData,fetching,detailing,detailContent,queryContent,querying };
};

const mapDispatchToProps = dispatch => ({
    fetchData:(page,pageSize,query) => {
        dispatch(actions.fetchData(page,pageSize,query))
    },
    detailData:()=>{
        dispatch(actions.detailData())
    },
    queryData:(query)=>{
        dispatch(actions.queryData(query))
    }
});

class Container extends Component {

    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);