/**
 * Created by Richie on 2018/4/16
 */
import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.QUERY_REQUEST:
            return state.merge({
                querying:false,
            });
        case actionTypes.QUERY_SUCCESS: {
            return state.merge({
                queryContent:action.data,
                querying:true,
            });
        }
        case actionTypes.QUERY_FAILURE:
            return state.merge({
                queryContent:{},
                querying:false,
            });
        default:
            return state;
    }
};