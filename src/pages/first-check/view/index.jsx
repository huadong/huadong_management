import React ,{Component}from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import { Form, Row, Col, Button ,DatePicker,Spin} from 'antd';
import Selection from '../../../share-component/select';
import CheckList from './check-list';
import getContent from '../../../utils/content';
import {withRouter} from 'react-router-dom';
import msg from '../../../utils/msg';
import config from '../../../config';
import {getMap,getVal} from '../../../utils/map';

const FormItem = Form.Item;
const { RangePicker }=DatePicker;

class AppComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            channel:"",
            sourceType:"",
            fromTime:null,
            toTime:null
        }

    }


    componentDidMount(){
        this.props.detailData();
    }

    componentWillUpdate(nextProps,nextState){
        if(this.props.querying!==nextProps.querying && nextProps.querying){
            console.error(nextProps.queryContent);
            if(!!nextProps.queryContent.articleDTO&&nextProps.queryContent.articleDTO.hasOwnProperty('id')){
                if(nextProps.queryContent.articleDTO.id===null){
                    msg(config.WARN,"当前查询条件下,暂无可审核文件！！");
                }else {
                    nextProps.history.push("/check-detail/"+nextProps.queryContent.articleDTO.id)
                }

            }else {
                msg();
                msg(config.SUCCESS,"已完成所有初审！！");

            }

        }
    }

    handleChange1 = (value)=>{
        this.setState({
            channel:value
        })
    };

    handleChange2 = (value)=>{
        this.setState({
            sourceType:value
        })
    };

    handleChange3 = (date,dateString)=>{
        console.log(date,dateString);
        this.setState({
            fromTime:moment(moment(date[0]).format(config.DAY)).format(config.TIME),
            toTime:moment(moment(date[1]).format(config.DAY)).format(config.TIME)
        })
    };

    handleRandomCheck = (e) => {
        e.preventDefault();
        const query = {};
        const {channel,sourceType,fromTime,toTime} = this.state;
        if(!!channel){query.channel = channel}
        if(!!sourceType){query.sourceType = sourceType}
        if(!!fromTime){query.fromTime = fromTime}
        if(!!toTime){query.toTime = toTime}
        console.log(query)
        this.props.queryData(query);
    };


    render() {
        const {entityData,fetching,fetchData,detailContent} = this.props;
        if(!detailContent.trialDTO){
            return (
                <Spin size="large" />
            )
        }else {
            msg();
            let channelMap = getMap(detailContent.map.channel);
            let sourceMap = getMap(detailContent.map.sourceType);
            console.log(channelMap,sourceMap);
            channelMap.unshift(config.AddALL);
            sourceMap.unshift(config.AddALL);
            return  (
                <div>
                    {getContent(<div className="content">
                        <h3>稿件审核概况</h3>
                        <br/>
                        <div>
                            <p>当前剩余未审核稿件：{detailContent.trialDTO.surplusArticle}</p>
                            <p>今日新增待审核稿件：{detailContent.trialDTO.addArticle}</p>
                            <p>今日完成稿件：{detailContent.trialDTO.finishedArticle}</p>
                        </div>

                        <Form className="ant-advanced-search-form">
                            <Row gutter={16} style={{display:"flex",alignItem:"center"}}>
                                <Col span={4}>
                                    <FormItem label="频道">
                                        <Selection onChange={(value)=>this.handleChange1(value)} msg={channelMap} dv=""/>
                                    </FormItem>
                                </Col>
                                <Col span={4}>
                                    <FormItem label="来源">
                                        <Selection onChange={(value)=>this.handleChange2(value)} msg={sourceMap} dv=""/>
                                    </FormItem>
                                </Col>
                                <Col span={8}>
                                    <FormItem label="时间">
                                        <RangePicker onChange={this.handleChange3}/>
                                    </FormItem>
                                </Col>
                                <Col span={4}>
                                    <FormItem>
                                        <Button type="primary"  onClick={this.handleRandomCheck}>进入审核</Button>
                                    </FormItem>
                                </Col>
                            </Row>
                        </Form>
                    </div>)}
                    <br/>
                    {getContent(<div className="content">
                        <h3>审核列表</h3>
                        <CheckList
                            entityData={entityData}
                            fetching={fetching}
                            fetchData={fetchData}
                            channelMap={channelMap}
                            sourceMap={sourceMap}
                        />
                    </div>)}
                </div>

            );
        }
    }
}

AppComponent.propTypes = {
    entityData:PropTypes.object.isRequired,
    fetching:PropTypes.bool.isRequired,
    fetchData:PropTypes.func.isRequired,

    detailContent:PropTypes.object.isRequired,
    detailing:PropTypes.bool.isRequired,
    detailData:PropTypes.func.isRequired,

    querying:PropTypes.bool.isRequired,
    queryData:PropTypes.func.isRequired
};

const WrapAppComponent = withRouter(AppComponent);

export default WrapAppComponent;
