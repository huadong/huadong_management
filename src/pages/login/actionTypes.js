import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    FETCHMENU_REQUEST: null,
    FETCHMENU_SUCCESS: null,
    FETCHMENU_FAILURE: null,
});