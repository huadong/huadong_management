import actionTypes from '../actionTypes';
import * as req from '../../../utils/request';
import config from '../../../config';
import { saveSession } from '../../../utils/auth';
import msg from '../../../utils/msg';

function fetching() {
    return{
        type:actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}

export default function fetchData(params) {

    return (dispatch) => {
        dispatch(fetching());
        req.postRequest(config.LogIn.userLogin, params).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                saveSession(responseJson);
                dispatch(fetched(responseJson));
                msg(config.LOAD,"登录中");
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(fetchFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.retCode.message);
            }
            else {
                dispatch(fetchFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.retCode.message);
            }
        }).catch((err) => {
            dispatch(fetchFail(err));
            console.error(err);
        });
    }
}