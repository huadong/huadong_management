import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken, saveSession, setStorage} from '../../../utils/auth';
import msg from '../../../utils/msg';

function fetchMenuing() {
    return {
        type: actionTypes.FETCHMENU_REQUEST
    };
}

function fetchMenued(data) {
    return {
        type: actionTypes.FETCHMENU_SUCCESS,
        data,
    };
}

function fetchMenuFail(error) {
    return {
        type: actionTypes.FETCHMENU_FAILURE,
        error,
    };
}

export default function fetchData(params) {

    return (dispatch) => {
        dispatch(fetchMenuing());
        console.log(params);
        debugger;
        req.getRequestHeader(config.LogIn.getUserMenu, params).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                setStorage("menu", responseJson.data);
                // saveSession(responseJson.data);
                // dispatch(fetchMenued(responseJson));
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(fetchMenuFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
            else {
                dispatch(fetchMenuFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.retCode.message);
            }
        }).catch((err) => {
            dispatch(fetchMenuFail(err));
            console.error(err);
        });
    }
}