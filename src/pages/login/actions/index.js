import fetchLogin from './fetch';
import fetchMenu from './fetchMenu';

export {
    fetchLogin,
    fetchMenu,
}