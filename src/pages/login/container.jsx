import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';


class Container extends Component {

    render() {
        return(
            <ViewComponent {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    const { fetching ,entityData} = state.login.toJS();
    return {
        entityData,
        fetching
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        fetchLogin: (params) => {
            dispatch(actions.fetchLogin(params))
        },
        fetchMenu: (params) => {
            dispatch(actions.fetchMenu(params))
        }
    }
};





export default connect(mapStateToProps, mapDispatchToProps)(Container);