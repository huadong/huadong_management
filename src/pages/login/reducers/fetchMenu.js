import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCHMENU_REQUEST:
            return state.merge({
                fetching:false,
            });
        case actionTypes.FETCHMENU_SUCCESS: {
            return state.merge({
                entityData:action.data,
                fetching:true,
            });
        }
        case actionTypes.FETCHMENU_FAILURE:
            return state.merge({
                entityData:{},
                fetching:false,
            });
        default:
            return state;
    }
};