import {fromJS} from 'immutable';
import reduceReducers from 'reduce-reducers';

import fetchLogin from './fetch';
import fetchMenu from './fetchMenu';

const initialState = fromJS({
    entityData:{},
    fetching:false
});

const reducer = reduceReducers(
    (state = initialState, action) => fetchLogin(state, action),
    (state = initialState, action) => fetchMenu(state, action),
);

export default reducer;