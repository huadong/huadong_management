import React from 'react';
import {Form, Input, Icon, Button, Layout} from 'antd';
import './login.css';
import LinkButton from '../../../share-component/link-button';
import msg from "../../../utils/msg";
import config from "../../../config";
import {withRouter} from 'react-router-dom';


const FormItem = Form.Item;
const {Header, Content} = Layout;

class Component extends React.Component {

    componentWillUpdate(nextProps, nextState) {
        if (this.props.fetching !== nextProps.fetching && nextProps.fetching) {
            if (!!nextProps.entityData && nextProps.entityData.hasOwnProperty('token')) {
                setTimeout(() => this.props.history.replace('/'), 1000)

            }

        }
        if (nextProps.entityData.token) {
            const data = {};
            data["X-Auth-Token"] = nextProps.entityData.token;
            this.props.fetchMenu(data);
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Layout>
                    <Header className="header" style={{
                        background: '#FFFFFF',
                        color: '#FFFFFF',
                        fontSize: '25px',
                        height: '60px',
                        cursor: 'pointer',
                        verticalAlign: 'middle'
                    }}>
                        <div>
                            <span> <Icon type="mail"/>&nbsp;内容审核管理后台</span>
                        </div>
                    </Header>
                    <div className="login" style={{"background": "#FFFFFF"}}>
                        <Content style={{height: "100vh"}}>
                            <Form className="login-form">
                                <h3 style={{textAlign: "center"}}>登录</h3>
                                <br/>
                                <FormItem>
                                    {getFieldDecorator("username", {
                                        rules: [{
                                            required: true, message: "请输入用户名／手机号"
                                        }]
                                    })(
                                        <Input prefix={<Icon type="mobile"/>} placeholder="输入手机号"/>)}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator("password", {
                                        rules: [{
                                            required: true, message: "请输入密码"
                                        }]
                                    })(
                                        <Input prefix={<Icon type="mail"/>} type="password" placeholder="输入登陆密码"/>)}
                                </FormItem>
                                <Button onClick={() => {
                                    this.props.form.validateFields((err, values) => {
                                        if (!err)
                                            this.props.fetchLogin(values);
                                    })
                                }}
                                        className="login-form-button"
                                        type="primary"
                                        style={{width: "250px"}}
                                >登录</Button>
                            </Form>
                        </Content>
                    </div>
                </Layout>
            </div>
        );
    }
}

const AppComponent = withRouter(Form.create()(Component));

AppComponent.defaultProps = {};

export default AppComponent;
