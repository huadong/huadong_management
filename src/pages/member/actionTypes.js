import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    GETSELECTION_REQUEST: null,
    GETSELECTION_SUCCESS: null,
    GETSELECTION_FAILURE: null,

    FETCHMEMBER_REQUEST: null,
    FETCHMEMBER_SUCCESS: null,
    FETCHMEMBER_FAILURE: null,
})