import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return {
        type: actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}

export default function fetchData(params) {
    return (dispatch) => {
        dispatch(fetching());
        const token = getToken();
        console.log(config.Record.getRecords);
        const {page, pageSize} = params;
        delete params["page"];
        delete params["pageSize"];
        let url = "";
        if (page) {
            url = "page=" + page;
        }
        if (pageSize) {
            url = url + "&pageSize=" + pageSize;
        }
        req.postRequestDetail(token, config.Record.getCheckMember + url, params).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data && responseJson.data.dataList.length !== 0) {

                    dispatch(fetched(responseJson.data));
                } else {
                    dispatch(fetched({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(fetchFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(fetchFail(err));
            console.error(err);
        });
    }
}