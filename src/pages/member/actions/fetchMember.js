import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetchMembering() {
    return {
        type: actionTypes.FETCHMEMBER_REQUEST
    };
}

function fetchMembered(data) {
    return {
        type: actionTypes.FETCHMEMBER_SUCCESS,
        data,
    };
}

function fetchMemberFail(error) {
    return {
        type: actionTypes.FETCHMEMBER_FAILURE,
        error,
    };
}

export default function fetchMem(params) {
    return (dispatch) => {
        dispatch(fetchMembering());
        const token = getToken();
        console.log(config.Record.getRecords);
        const {page, pageSize} = params;
        delete params["page"];
        delete params["pageSize"];
        let url = "";
        if (page) {
            url = "page=" + page;
        }
        if (pageSize) {
            url = url + "&pageSize=" + pageSize;
        }
        req.postRequestDetail(token, config.Record.getMemberDetail + url, params).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data && responseJson.data.dataList.length !== 0) {

                    dispatch(fetchMembered(responseJson.data));
                } else {
                    dispatch(fetchMembered({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(fetchMemberFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(fetchMemberFail(err));
            console.error(err);
        });
    }
}