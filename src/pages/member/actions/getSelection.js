import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function getting() {
    return {
        type: actionTypes.GETSELECTION_REQUEST,
    };
}

function getted(data) {
    return {
        type: actionTypes.GETSELECTION_SUCCESS,
        data,
    };
}

function getFail(error) {
    return {
        type: actionTypes.GETSELECTION_FAILURE,
        error,
    };
}

export default function getSelection() {
    return (dispatch) => {
        dispatch(getting());
        const token = getToken();
        req.getRequestDetail(token, config.Record.getSelection).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                (responseJson.data) ? (
                    dispatch(getted(responseJson.data))
                ) : (
                    dispatch(getted({}))
                )
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(getFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(getFail(err));
            console.error(err);
        });
    }
}