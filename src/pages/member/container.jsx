import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {AppComponent, MemberHistory} from './view';
import * as actions from './actions';



class MemberComponent extends React.Component {

    render() {

        return(
            <AppComponent {...this.props}/>
        )
    }
}

class MemberHistoryComponent extends React.Component {

    render() {

        return(
            <MemberHistory {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.memCheck.data,
        fetching: state.memCheck.fetching,
        // title: state.record.title,
        // counting: state.record.counting,
        selection: state.memCheck.selection,
        // history: state.record.history,
        memberInfo: state.memCheck.memberInfo,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params));
        },
        // countData: (params) => {
        //     dispatch(actions.countData(params));
        // },
        getSelection: () => {
            dispatch(actions.getSelection());
        },
        // getRecordHistory: (params) => {
        //     dispatch(actions.getRecordHistory(params));
        // }
        fetchMember: (params) => {
            dispatch(actions.fetchMember(params))
        }
    }
}

const Member = connect(mapStateToProps, mapDispatchToProps)(MemberComponent);
const MemberCheckHistory = connect(mapStateToProps, mapDispatchToProps)(MemberHistoryComponent);

export {
    Member,
    MemberCheckHistory,
}


