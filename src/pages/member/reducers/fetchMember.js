import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCHMEMBER_REQUEST:
            return {
                ...state,
                fetched: false,
            }
        case actionTypes.FETCHMEMBER_SUCCESS:
            return {
                ...state,
                fetched: true,
                memberInfo: action.data,
            }
        case actionTypes.FETCHMEMBER_FAILURE:
            return {
                ...state,
                fetched: false,
            }
        default:
            return state;
    }

}