import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.GETSELECTION_REQUEST:
            return {
                ...state,
                getted: false,
            }
        case actionTypes.GETSELECTION_SUCCESS:
            return {
                ...state,
                getted: true,
                selection: action.data,
            }
        case actionTypes.GETSELECTION_FAILURE:
            return {
                ...state,
                getted: false,
            }
        default:
            return state;
    }

}