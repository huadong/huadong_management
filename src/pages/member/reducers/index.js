import getSelection from './getSelection';
import fetchData from './fetch';
import fetchMember from "./fetchMember";

import reduceReducers from 'reduce-reducers';

const initialState = {
    data: {},
    fetching: false,
    selection: {},
    getting: false,
    memberInfo: {},
}

const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    // (state = initialState, action) => countData(state, action),
    (state = initialState, action) => getSelection(state, action),
    // (state = initialState, action) => getRecordHistory(state, action),
    (state = initialState, action) => fetchMember(state, action),
);


export default reducer