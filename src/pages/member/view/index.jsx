import React from 'react';
import Component from './member';
import Member from './recordMemberHistory';
import getContent from '../../../utils/content';


export class AppComponent extends React.Component {


    render() {

        return(
            getContent(
                <div className="content">
                    <Component {...this.props}/>
                </div>
            )
        )
    }
}

export class MemberHistory extends React.Component {


    render() {

        return(
            getContent(
                <div className="content">
                    <Member {...this.props}/>
                </div>
            )
        )
    }
}

