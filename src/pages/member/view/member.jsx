import React from 'react';
import {Col, Row, DatePicker, Input, Button, Table, Form, LocaleProvider} from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';


const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 14},
}

const columns = [{
    title: '成员ID',
    dataIndex: 'createUserId',
    width: "200px"
}, {
    title: '成员名称',
    dataIndex: 'createUserName',
    width: "200px"
}, {
    title: '网络图文',
    width: "400px",
    render: (props, row) => {
        const count = row.count.split(',');
        const result = row.result.split(',');
        const param = {}
        result.map((item, i) => {
            param[`${item}`] = count[i];
        })
        //retrialPassWeight: "3", trialReject: "3", trialPass: "18"
        const trialPass = param.trialPass || 0;
        const retrialPassWeight = param.retrialPassWeight || 0;
        const trialReject = param.trialReject || 0;
        return (
            <span>
                通过：{trialPass} 驳回：{retrialPassWeight} 废弃：{trialReject}
            </span>
        )
    }
}, {
    title: '',
    dataIndex: 'action',
    render: (props, row) => (
        <a href={"/record-member-history/" + row.createUserId}>审核记录查询</a>
    )
}];
const data = [
    {
        key: "0",
        id: "1",
        name: "2",
        interArticle: "4",
    }
];

class member extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            pagination: {
                count: 1,
            },
            loading: false,
            params: {},
        }
    }

    componentDidMount() {
        this.props.fetchData({page: 1})
    }

    shouldChange = (par) => {
        const {params} = this.state;
        for (let o in par) {
            if (par[o] != params[o]) {
                this.setState({
                    params: par,
                })
                return false;
            }
        }
        return true;
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        const {count} = this.state.pagination;
        if (nextProps) {
            if (nextProps) {
                if (this.shouldChange(nextProps.form.getFieldsValue())) {
                    this.setState({
                        data: nextProps.data,
                        pagination: {
                            pageSize: nextProps.data.pageSize,
                            total: nextProps.data.totalCount,
                            count: count,
                        },
                        loading: true,
                    }, () => {
                        this.setState({
                            loading: false,
                        })
                    })
                }
            }
        }
    }

    handleSearch = () => {
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        for (let o in params) {
            if (o == "createTime")
                params[o] = params[o].format("YYYY-MM-DD 00:00:00")
        }
        this.props.fetchData({...params, page: 1});
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        for (let o in params) {
            if (o == "createTime")
                params[o] = params[o].format("YYYY-MM-DD 00:00:00")
        }
        this.props.fetchData({
            ...params,
            page: pagination.current,
        });
    }

    render() {
        console.log(this.state);
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Form layout="horizontal">
                    <Row>
                        <Col span={6}>
                            <FormItem label="时间"
                                      {...formItemLayout}
                            >

                                <LocaleProvider locale={zh_CN}>
                                    {getFieldDecorator('createTime')(
                                        <DatePicker/>)}
                                </LocaleProvider>
                            </FormItem>
                        </Col>
                        <Col span={6}>

                            <FormItem label="人员"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator('createUserName', {})
                                (
                                    <Input placeholder="请输入人员名称"/>)}
                            </FormItem>
                        </Col>
                        <Col span={6}>

                            <FormItem label="ID"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator("createUserId")(
                                    <Input placeholder="请输入ID"/>)}
                            </FormItem>
                        </Col>
                        <Col span={6}>
                            <Button style={{marginTop: "4px"}} onClick={this.handleSearch}>查询</Button>
                        </Col>
                    </Row>
                </Form>
                <Table columns={columns}
                       dataSource={this.state.data.dataList}
                       loading={this.state.loading}
                       onChange={this.handleTableChange}
                       pagination={this.state.pagination}
                       rowKey="createUserId"
                />
            </div>
        )
    }
}

const AppComponent = Form.create()(member)

AppComponent.defaultProps = {};

export default AppComponent;
