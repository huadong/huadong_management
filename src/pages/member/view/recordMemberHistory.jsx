import React from 'react';
import {Col, Row, DatePicker, Input, Button, Table, Form, LocaleProvider, Select} from 'antd';
import LinkButton from '../../../share-component/link-button';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';

const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {span: 7},
    wrapperCol: {span: 14},
}

const columns = [{
    title: '标题',
    dataIndex: 'articleTitle',
    width: "200px"
}, {
    title: '创建时间',
    dataIndex: 'sourceReleaseTime',
    width: "200px"
}, {
    title: '提交时间',
    dataIndex: 'trialTime',
    width: "200px"
}, {
    title: '初审状态',
    dataIndex: 'trialState',
    width: "200px",
    render: props => {
        switch (props){
            case "awaitTrial":
                return "待初审";
            case "trialPass":
                return "已通过";
            case "trialReject":
                return "已驳回";
        }
    }
}, {
    title: '复审状态',
    dataIndex: 'retrialState',
    width: "200px",
    render: props => {
        switch (props){
            case "retrialPassWeight":
                return "已通过并加权";
            case "retrialPass":
                return "已通过";
            case "retrialReject":
                return "已驳回";
            case "retrialRejectDiscard":
                return "已驳回并废弃";
        }
    }
}, {
    title: '操作',
    dataIndex: 'action',
    render: (props, row) => (
        <LinkButton title="历史记录"
                    pathName='/record-history'
                    id={row.articleId}/>
    )
}];
const data = [
    {
        key: "0",
        id: "1",
        name: "2",
        interArticle: "4",
    }
];

class member extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            params: {},
            pagination: {
                count: 1,
            },
            data: {

            }
        }
    }

    componentDidMount() {
        this.props.fetchMember({page: 1, createUserId: this.props.match.params.id})
        this.props.getSelection();
    }

    shouldChange = (par) => {
        const {params} = this.state;
        for (let o in par) {
            if (par[o] != params[o]) {
                this.setState({
                    params: par,
                    pagination: {
                        count: 1,
                    }
                })
                return false;
            }
        }
        return true;
    }

    componentWillReceiveProps(nextProps) {
        const {count} = this.state.pagination;
        console.log(nextProps);
        if (nextProps) {
            if (this.shouldChange(nextProps.form.getFieldsValue())) {
                this.setState({
                    data: nextProps.memberInfo,
                    pagination: {
                        pageSize: nextProps.memberInfo.pageSize,
                        total: nextProps.memberInfo.totalCount,
                        count: count,
                    },
                    loading: true,
                }, () => {
                    this.setState({
                        loading: false,
                    })
                })
            }
        }
    }

    handleSelection = (params) => {
        const array = [];
        for (let o in params) {
            if (o.indexOf("retrial") == 0)
                continue;
            array.push(
                <Option value={o} key={o}>
                    {params[o]}
                </Option>
            )
        }
        return array;
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        const params = this.props.form.getFieldsValue();
        for(let o in params){
            if(!params[o])
                delete params[o];
        }
        for(let o in params){
            if(o == "trialTime") {
                params[o] = params[o].format("YYYY-MM-DD 00:00:00")
            }
        }
        this.props.fetchMember({...params, page: pagination.current, createUserId: this.props.match.params.id})
    }

    handleClick = () => {
        const params = this.props.form.getFieldsValue();
        for(let o in params){
            if(!params[o])
                delete params[o];
        }
        for(let o in params){
            if(o == "trialTime") {
                params[o] = params[o].format("YYYY-MM-DD 00:00:00")
            }
        }
        this.props.fetchMember({...params, page: 1, createUserId: this.props.match.params.id})
    }

    render() {
        console.log(this.state);
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Form layout="horizontal">
                    <Row>
                        <Col span={5}>
                            <FormItem label="时间"
                                      {...formItemLayout}
                            >

                                <LocaleProvider locale={zh_CN}>
                                    {getFieldDecorator('trialTime', {}
                                    )(
                                        <DatePicker/>)}
                                </LocaleProvider>
                            </FormItem>
                        </Col>
                        <Col span={5}>

                            <FormItem label="标题"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator('title', {})
                                (
                                    <Input placeholder="请输入标题"/>)}
                            </FormItem>
                        </Col>
                        <Col span={5}>

                            <FormItem label="频道"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator("channel")(
                                    <Select placeholder="请选择频道" allowClear>
                                        {this.props.selection.channel ? this.handleSelection(this.props.selection.channel) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={5}>

                            <FormItem label="初审状态"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator("trialState")(
                                    <Select placeholder="请选择初审状态" allowClear>
                                        {this.props.selection.state ? this.handleSelection(this.props.selection.state) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <Button style={{marginTop: "4px"}} type="primary" onClick={this.handleClick}>查询</Button>
                        </Col>
                    </Row>
                </Form>
                <Table
                    columns={columns}
                    dataSource={this.state.data.dataList}
                    loading={this.state.loading}
                    rowKey="articleId"
                    pagination={this.state.pagination}
                    onChange={this.handleTableChange}
                />
            </div>
        )
    }
}

export default Form.create()(member);