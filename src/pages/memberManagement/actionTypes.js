import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    GETSELECTION_REQUEST: null,
    GETSELECTION_SUCCESS: null,
    GETSELECTION_FAILURE: null,

    ADD_REQUEST: null,
    ADD_SUCCESS: null,
    ADD_FAILURE: null,

    ABLE_REQUEST: null,
    ABLE_SUCCESS: null,
    ABLE_FAILURE: null,

    GETINFO_REQUEST: null,
    GETINFO_SUCCESS: null,
    GETINFO_FAILURE: null,
})