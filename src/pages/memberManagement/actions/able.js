import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';

function abling() {
    return {
        type: actionTypes.ABLE_REQUEST
    };
}

function abled(data) {
    return {
        type: actionTypes.ABLE_SUCCESS,
        data,
    };
}

function ableFail(error) {
    return {
        type: actionTypes.ABLE_FAILURE,
        error,
    };
}

export default function addData(params) {

    return (dispatch) => {
        dispatch(abling());
        const token = getToken();
        console.log(config.Sensitive.querySensitiveWords);
        const url = params.action ? config.PersonalCenter.ableMember : config.PersonalCenter.disableMember;
        req.postRequestDetail(token, url + "id=" + params.id).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                if (responseJson.data) {

                    dispatch(abled(responseJson.retCode.message));
                } else {
                    dispatch(abled({}))
                }
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(ableFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
            else {
                dispatch(ableFail(responseJson.retCode.message));
            }
        }).catch((err) => {
            dispatch(ableFail(err));
            console.error(err);
        });
    }
}