import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';

function adding() {
    return {
        type: actionTypes.ADD_REQUEST
    };
}

function added(data, action) {
    return {
        type: actionTypes.ADD_SUCCESS,
        data,
        action,
    };
}

function addFail(error) {
    return {
        type: actionTypes.ADD_FAILURE,
        error,
    };
}

export default function addData(params, action) {

    return (dispatch) => {
        dispatch(adding());
        const token = getToken();
        console.log(config.Sensitive.querySensitiveWords);
        const url = action == "add" ? config.PersonalCenter.addMember : config.PersonalCenter.modifyMember;
        const request = action == "add" ? req.postRequestDetail : req.putRequestDetail;
        request(token, url, params).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                if (responseJson.retCode.message) {

                    dispatch(added(responseJson.retCode.message, action));
                } else {
                    dispatch(added({}))
                }
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(addFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.message);
            }
            else {
                dispatch(addFail(responseJson.retCode.message));
            }
        }).catch((err) => {
            dispatch(addFail(err));
            console.error(err);
        });
    }
}