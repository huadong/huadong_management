import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function getInfoing() {
    return {
        type: actionTypes.GETINFO_REQUEST,
    };
}

function getInfoed(data) {
    return {
        type: actionTypes.GETINFO_SUCCESS,
        data,
    };
}

function getInfoFail(error) {
    return {
        type: actionTypes.GETINFO_FAILURE,
        error,
    };
}

export default function getInfo(params) {
    return (dispatch) => {
        dispatch(getInfoing());
        const token = getToken();
        req.getRequestDetail(token, config.PersonalCenter.getPersonalInfo + params.id).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                (responseJson.data) ? (
                    dispatch(getInfoed(responseJson.data))
                ) : (
                    dispatch(getInfoed({}))
                )
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(getInfoFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(getInfoFail(err));
            console.error(err);
        });
    }
}