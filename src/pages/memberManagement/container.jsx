import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

class Container extends Component {
    componentDidMount(){

    }

    render() {
        return(
            <ViewComponent {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        memInfo: state.memMana.memInfo,
        fetching: state.memMana.fetching,
        selection: state.memMana.selection,
        addMessage: state.memMana.addMessage,
        added: state.memMana.added,
        info: state.memMana.info,
        action: state.memMana.action,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params))
        },
        getSelection: () => {
            dispatch(actions.getSelection())
        },
        addData: (params, action) => {
            dispatch(actions.addData(params, action))
        },
        ableMember: (params) => {
            dispatch(actions.ableMember(params))
        },
        getInfo: (params) => {
            dispatch(actions.getInfo(params))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);