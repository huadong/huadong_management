import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.ABLE_REQUEST:
            return {
                ...state,
            }
        case actionTypes.ABLE_SUCCESS:
            return {
                ...state,
            }
        case actionTypes.ABLE_FAILURE:
            return {
                ...state,
            }
        default:
            return state;
    }

}