import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.GETINFO_REQUEST:
            return {
                ...state,
            }
        case actionTypes.GETINFO_SUCCESS:
            return {
                ...state,
                info: action.data,
            }
        case actionTypes.GETINFO_FAILURE:
            return {
                ...state,
            }
        default:
            return state;
    }

}