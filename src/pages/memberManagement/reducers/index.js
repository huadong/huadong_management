import fetchData from './fetch';
import addData from './add';
import getSelection from './getSelection';
import ableMember from './able';
import getInfo from './getInfo';
import reduceReducers from 'reduce-reducers';


const initialState = {
    memInfo: {},
    fetching: false,
    addMessage: "",
    modifyMessage: "",
    selection: {},
    added: true,
    info: {},
    action: ""
}


const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => addData(state, action),
    (state = initialState, action) => getSelection(state, action),
    (state = initialState, action) => ableMember(state, action),
    (state = initialState, action) => getInfo(state, action),
)

export default reducer;