import React from 'react';
import {Input, Select, Form, Row, Col} from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
    labelCol: {span: 4},
    wrapperCol: {span: 20}
}

class AddMember extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            realName: "",
            nickName: "",
            phone: "",
            roleCode: "",
        }
    }

    componentDidMount() {
        console.log(this.props);
        this.props.getSelection();
        if(this.props.id){
            this.props.getInfo({id: this.props.id});
        }
    }

    handleOption = (params) => {
        const array = [];
        for(let o in params) {
            array.push(
                <Option value={o} key={o}>
                    {params[o]}
                </Option>
            )
        }
        return array;
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.info)
        {
            if(JSON.stringify(nextProps.info) != '{}')
            {
                this.setState({
                    realName: nextProps.info.realName,
                    nickName: nextProps.info.nickName,
                    phone: nextProps.info.phone,
                    roleCode: nextProps.info.roleCode,
                })
            }
        }
    }

    handleData = () => {
        if(this.props.id){
            const values = this.props.form.validateFields((err, values) => {
                if(!err){
                    return values;
                }
            })
            this.props.handleInfo(values);
        }
    }

    render() {

        const {getFieldDecorator} = this.props.form;

        if(this.props.handleInfo) {
            this.props.handleInfo(this.props.form.getFieldsValue());
        }

        return (
            <div>
                <Form>
                    <FormItem label="成员姓名"
                              {...formItemLayout}
                    >
                        {
                            getFieldDecorator("realName", {
                                // rules: [{
                                //     required: true, message: "请输入成员姓名"
                                // }],
                                initialValue: this.state.realName,
                            })(
                                <Input placeholder="请输入成员姓名"/>)}
                    </FormItem>
                    <FormItem label="成员昵称"
                              {...formItemLayout}
                    >
                        {
                            getFieldDecorator("nickName", {
                                // rules: [{
                                //     required: true, message: "请输入昵称"
                                // }],
                                initialValue: this.state.nickName,
                            })(
                                <Input placeholder="请输入昵称"/>)}
                    </FormItem>
                    <FormItem label="手机号"
                              {...formItemLayout}
                    >
                        {
                            getFieldDecorator("phone", {
                                // rules: [{
                                //     required: true, message: "请输入电话号码"
                                // }],
                                initialValue: this.state.phone,
                            })(
                                <Input placeholder="请输入电话号码"/>)}
                    </FormItem>
                    <FormItem label="登录密码"
                              {...formItemLayout}
                    >
                        {
                            getFieldDecorator("password", {
                                // rules: [{
                                //     required: true, message: "请输入密码"
                                // }],
                            })(
                                <Input placeholder="请输入密码"/>)}
                    </FormItem>
                    <FormItem label="所属角色"
                              {...formItemLayout}
                    >
                        {
                            getFieldDecorator("roleCode", {
                                // rules: [{
                                //     required: true, message: "请选择所属角色"
                                // }],
                                initialValue: this.state.roleCode == "" ? null : this.state.roleCode,
                            })(
                                <Select placeholder="请选择所属角色"
                                        getPopupContainer={triggerNode => triggerNode.parentNode}
                                >
                                    {this.props.selection ? this.handleOption(this.props.selection) : null}
                                </Select>
                                    )}
                    </FormItem>
                </Form>
            </div>
        )
    }
}

export default Form.create()(AddMember);