import React from 'react';
import MemberManagement from './memberManagement';
import getContent from '../../../utils/content';


export default class AppComponent extends React.Component {


    render() {

        return(
            getContent(
                <div className="content">
                    <MemberManagement {...this.props}/>
                </div>)
        )
    }
}

