import React from 'react';
import MultiButton from '../../../share-component/multi-button';
import {Table, Switch, Modal} from 'antd';
import AddMember from './addMember';


let params;

export default class MemberManagement extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            loading: false,
            pagination: {
                count: 1,
            },
        }
    }

    componentDidMount() {
        console.log(this.props);
        this.props.fetchData({});
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        const {count} = this.state.pagination;
        if (nextProps) {
            this.setState({
                data: nextProps.memInfo,
                loading: true,
                pagination: {
                    pageSize: nextProps.memInfo.pageSize,
                    total: nextProps.memInfo.totalCount,
                    count: count,
                },
            }, () => {
                this.setState({
                    loading: false,
                })
            })
        }
        if (nextProps.addMessage && nextProps.addMessage != "") {
            if (nextProps.addMessage == "OK") {
                const message = nextProps.action == "add" ? "添加成功" : "修改成功";
                Modal.success({
                    title: message
                })
            }
            else {
                Modal.error(({
                    title: nextProps.addMessage,
                }))
            }
            this.props.fetchData({
                page: this.state.pagination.count,
            });
        }
    }

    handleSwitch = (id, value) => {
        console.log(value);
        console.log(id);
        this.props.ableMember({action: value, id: id});
        // this.props.fetchData({
        //     page: this.state.pagination.count
        // })
    }

    handleOk = (id, state) => {
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        this.props.addData({...params, id: id, state: state}, "modify");
    }

    handleOkTotal = () => {
        const params = this.refs.addMemberTotal.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o]
        }
        this.props.addData(params, "add");
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        this.props.fetchData({
            page: pagination.current,
        });
    }

    handleInfo = (param) => {
        params = param;
    }

    render() {

        const columns = [{
            title: '成员id',
            dataIndex: 'id',
            width: "200px"
        }, {
            title: '成员姓名',
            dataIndex: 'realName',
            width: "200px"
        }, {
            title: '昵称',
            dataIndex: 'nickName',
            width: "200px"
        }, {
            title: '手机号',
            dataIndex: 'username',
            width: "200px"
        }, {
            title: '所属角色',
            dataIndex: 'roleName',
            width: "200px"
        }, {
            title: '添加时间',
            dataIndex: 'addTime',
            width: "200px"
        }, {
            title: '状态',
            width: "150px",
            dataIndex: 'state',
            render: (props, row) => {
                const par = props == 1 ? true : false;
                return (
                    <div>
                        <Switch checkedChildren="生效中"
                                unCheckedChildren="已失效"
                                defaultChecked={par}
                                onChange={this.handleSwitch.bind(this, row.id)}
                        />
                    </div>
                )
            }
        }, {
            title: '操作',
            render: (props, row) => {
                return (
                    <div>
                        <MultiButton
                            title="修改"
                            type="primary"
                            modalTitle="修改角色"
                            modalText="修改角色"
                            select={true}
                            handleOk={this.handleOk.bind(this, row.id, row.state)}
                        >
                            <AddMember getSelection={this.props.getSelection}
                                       selection={this.props.selection}
                                       data={row}
                                       ref="addMember"
                                       handleInfo={this.handleInfo}
                                       id={row.id}
                                       state={row.state}
                                       getInfo={this.props.getInfo}
                                       info={this.props.info}
                            />
                        </MultiButton>
                    </div>
                )
            }
        }];

        return (
            <div>
                <MultiButton center={true}
                             type="primary"
                             title="添加成员"
                             modalTitle="添加成员"
                             select={true}
                             handleOk={this.handleOkTotal}
                             right={true}
                >
                    <AddMember getSelection={this.props.getSelection}
                               selection={this.props.selection}
                               ref="addMemberTotal"
                    />
                </MultiButton>
                <Table columns={columns}
                       dataSource={this.state.data.records}
                       onChange={this.handleTableChange}
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                       rowKey="id"
                />
            </div>
        )
    }
}