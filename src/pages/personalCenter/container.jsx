import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

class Container extends Component {
    componentDidMount(){

    }

    render() {
        return(
            <ViewComponent {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        info: state.personalCenter.info,
        fetching: state.personalCenter.fetching,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);