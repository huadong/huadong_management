import fetchData from './fetch';
import reduceReducers from 'reduce-reducers';


const initialState = {
    info: {},
    fetching: false,
}


const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
)

export default reducer;