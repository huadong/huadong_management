import PersonalCenter from './personalCenter';
import getContent from '../../../utils/content';
import React from "react";


export default class AppComponent extends React.Component {


    render() {
        return(
            getContent(
                <div className="content">
                    <PersonalCenter params={this.props.match.params} {...this.props}/>
                </div>)
        )
    }
}