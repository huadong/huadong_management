import React from 'react';
import {Form, Col, Row} from 'antd';

const FormItem = Form.Item;
const formItemLayout = {
    labelCol: {span: 2},
    wrapperCol: {span: 21},
}

export default class PersonalCenter extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            info: {}
        }
    }

    componentDidMount(){
        console.log(this.props);
        this.props.fetchData({id: 1});
    }

    componentWillReceiveProps(nextProps){
        if(nextProps) {
            this.setState({
                info: nextProps.info
            })
        }
    }

    render() {
        const {info} = this.state;
        return(
            <div>
                <Form>
                    <Row>
                        <Col>
                            <FormItem label="昵称"
                                      {...formItemLayout}
                            >
                                {info.nickName ? info.nickName : null}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormItem label="姓名"
                                      {...formItemLayout}
                            >
                                {info.realName ? info.realName : null}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormItem label="手机号"
                                      {...formItemLayout}
                            >
                                {info.phone ? info.phone : null}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}