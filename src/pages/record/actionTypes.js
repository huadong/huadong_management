import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    COUNT_REQUEST: null,
    COUNT_SUCCESS: null,
    COUNT_FAILURE: null,

    GETSELECTION_REQUEST: null,
    GETSELECTION_SUCCESS: null,
    GETSELECTION_FAILURE: null,

    FETCHHISTORY_REQUEST: null,
    FETCHHISTORY_SUCCESS: null,
    FETCHHISTORY_FAILURE: null,
})