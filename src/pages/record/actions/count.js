import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';

function counting() {
    return {
        type: actionTypes.COUNT_REQUEST
    };
}

function counted(data) {
    return {
        type: actionTypes.COUNT_SUCCESS,
        data,
    };
}

function countFail(error) {
    return {
        type: actionTypes.COUNT_FAILURE,
        error,
    };
}

export default function countData(params) {

    return (dispatch) => {
        dispatch(counting());
        const token = getToken();
        console.log(config.Record.getRecords);
        req.getRequestDetail(token, config.Record.getRecordSurvey, params).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data) {
                    dispatch(counted(responseJson.data));
                } else {
                    dispatch(counted({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(countFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(countFail(err));
            console.error(err);
        });
    }
}