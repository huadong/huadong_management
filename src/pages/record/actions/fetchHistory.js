import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetchHistorying() {
    return {
        type: actionTypes.FETCHHISTORY_REQUEST
    };
}

function fetchHistoryed(data) {
    return {
        type: actionTypes.FETCHHISTORY_SUCCESS,
        data,
    };
}

function fetchHistoryFail(error) {
    return {
        type: actionTypes.FETCHHISTORY_FAILURE,
        error,
    };
}

export default function fetchData(params) {
    return (dispatch) => {
        dispatch(fetchHistorying());
        const token = getToken();
        console.log(config.Record.getRecordHistory);
        const {id, page, pageSize} = params;
        let url = "";
        url = "articleId=" + id;
        if (page)
            url = url + "&page=" + page;
        if (pageSize)
            url = url + "&pageSize" + pageSize;
        req.getRequestDetail(token, config.Record.getRecordHistory + url).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data && responseJson.data.dataList.length !== 0) {

                    dispatch(fetchHistoryed(responseJson.data));
                } else {
                    dispatch(fetchHistoryed({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(fetchHistoryFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(fetchHistoryFail(err));
            console.error(err);
        });
    }
}