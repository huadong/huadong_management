import fetchData from './fetch';
import countData from './count';
import getSelection from './getSelection';
import getRecordHistory from './fetchHistory';

export {
    fetchData,
    countData,
    getSelection,
    getRecordHistory,
}