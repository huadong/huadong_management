import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {RecordTemp, RecordHistoryTemp} from './view';
import * as actions from './actions';

class RecordComponent extends React.Component {

    render() {

        return(
            <RecordTemp {...this.props}/>
        )
    }
}

class RecordHistoryComponent extends React.Component {

    render() {

        return(
            <RecordHistoryTemp {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.record.data,
        fetching: state.record.fetching,
        title: state.record.title,
        counting: state.record.counting,
        selection: state.record.selection,
        history: state.record.history,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params));
        },
        countData: (params) => {
            dispatch(actions.countData(params));
        },
        getSelection: () => {
            dispatch(actions.getSelection());
        },
        getRecordHistory: (params) => {
            dispatch(actions.getRecordHistory(params));
        }
    }
}


const Record = connect(mapStateToProps, mapDispatchToProps)(RecordComponent);
const RecordHistory = connect(mapStateToProps, mapDispatchToProps)(RecordHistoryComponent);

export {
    Record,
    RecordHistory
}