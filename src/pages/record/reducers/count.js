import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.COUNT_REQUEST:
            return {
                ...state,
                counting: false,
            }
        case actionTypes.COUNT_SUCCESS:
            return {
                ...state,
                counting: true,
                title: action.data,
                addContent: {
                    message: "",
                },
                actionContent: {
                    message: "",
                }
            }
        case actionTypes.COUNT_FAILURE:
            return {
                ...state,
                counting: false,
            }
        default:
            return state;
    }

}