import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.FETCHHISTORY_REQUEST:
            return {
                ...state,
                fetchhistorying: false,
            }
        case actionTypes.FETCHHISTORY_SUCCESS:
            return {
                ...state,
                fetchhistorying: true,
                history: action.data,
                addContent: {
                    message: "",
                },
                actionContent: {
                    message: "",
                }
            }
        case actionTypes.FETCHHISTORY_FAILURE:
            return {
                ...state,
                fetchhistorying: false,
            }
        default:
            return state;
    }

}