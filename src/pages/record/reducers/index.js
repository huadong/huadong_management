import fetchData from './fetch';
import countData from './count';
import getSelection from './getSelection';
import getRecordHistory from './fetchHistory';
import reduceReducers from 'reduce-reducers';


const initialState = {
    data: {},
    fetching: false,
    title: {},
    counting: false,
    selection: {},
    getting: false,
    history: {},
    fetchhistorying: false,
}


const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => countData(state, action),
    (state = initialState, action) => getSelection(state, action),
    (state = initialState, action) => getRecordHistory(state, action),
);


export default reducer