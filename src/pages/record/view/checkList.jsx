import React from 'react';
import {DatePicker, Input, Select, Button, Table, Form, Col, Divider, Row, LocaleProvider} from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';

import LinkButton from '../../../share-component/link-button';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
    labelCol: {span: 7},
    wrapperCol: {span: 10},
}
const formItemLayout2 = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
}
const columns = [{
    title: '最后审核时间',
    dataIndex: 'lastModifyTime',
    width: "150px"
}, {
    title: '标题',
    dataIndex: 'articleTitle',
    width: "200px"
}, {
    title: '创建时间',
    dataIndex: 'createTime',
    width: "150px"
}, {
    title: '初审人员',
    dataIndex: 'trialUsername',
    width: "100px"
}, {
    title: '提交时间',
    dataIndex: 'trialTime',
    width: "100px"
}, {
    title: '初审状态',
    dataIndex: 'trialState',
    width: "100px",
    render: props => {
        switch (props){
            case "awaitTrial":
                return "待初审";
            case "trialPass":
                return "已通过";
            case "trialReject":
                return "已驳回";
        }
    }
}, {
    title: '复审状态',
    dataIndex: 'retrialState',
    width: "100px",
    render: props => {
        switch (props){
            case "retrialPassWeight":
                return "已通过并加权";
            case "retrialPass":
                return "已通过";
            case "retrialReject":
                return "已驳回";
            case "retrialRejectDiscard":
                return "已驳回并废弃";
        }
    }
}, {
    title: '操作',
    render: (props, row) => {
        return (
            <div>
                <Button size="small"
                    disabled={row.retrialState == "retrialRejectDiscard" ? true : false}
                >进入复审</Button>
                <LinkButton title="历史记录"
                            pathName='/record-history'
                            id={props.articleId}
                            size="small"
                />
            </div>
        )
    }
}];
const data = [
    {
        key: "0",
        words: "1",
        range: "2",
        updateTime: "4",
        status: "3",
    }
];

class CheckListTemp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            pagination: {
                count: 1,
            },
            params: {},
            loading: false,
        }
    }

    componentDidMount() {
        this.props.fetchData({page: 1});
        this.props.getSelection();
    }

    shouldChange = (par) => {
        const {params} = this.state;
        for (let o in par) {
            if (par[o] != params[o]) {
                this.setState({
                    params: par,
                })
                return false;
            }
        }
        return true;
    }

    componentWillReceiveProps(nextProps) {
        const {count} = this.state;
        if (nextProps) {
            if (this.shouldChange(nextProps.form.getFieldsValue())) {
                this.setState({
                    data: nextProps.data,
                    pagination: {
                        pageSize: nextProps.data.pageSize,
                        total: nextProps.data.totalCount,
                        count: count,
                    },
                    loading: true,
                }, () => {
                    this.setState({
                        loading: false,
                    })
                })
            }
        }
    }

    handleSearch = () => {
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        if (params.trialTime) {
            params.trialTime = params.trialTime.format("YYYY-MM-DD 00:00:00");
        }
        this.props.fetchData(params);
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        if (params.trialTime) {
            params.trialTime = params.trialTime.format("YYYY-MM-DD 00:00:00");
        }
        this.props.fetchData({
            ...params,
            page: pagination.current,
        });
    }

    handleSelection = (params) => {
        const array = [];
        for (let o in params) {
            if (o.indexOf("retrial") == 0)
                continue;
            array.push(
                <Option value={o} key={o}>
                    {params[o]}
                </Option>
            )
        }
        return array;
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <div>
                    <Form layout="horizontal">
                        <Row>
                            <Col span={4}>
                                <FormItem label="时间"
                                          {...formItemLayout2}
                                >
                                    <LocaleProvider locale={zh_CN}>
                                        {getFieldDecorator("trialTime")(
                                            <DatePicker/>)}
                                    </LocaleProvider>
                                </FormItem>
                            </Col>
                            <Col span={4}>
                                <FormItem label="人员"
                                          {...formItemLayout2}
                                >
                                    {getFieldDecorator("trialName")(
                                        <Input placeholder="人员姓名"/>)}
                                </FormItem>
                            </Col>
                            <Col span={4}>
                                <FormItem label="标题"
                                          {...formItemLayout2}
                                >
                                    {getFieldDecorator("articleTitle")(
                                        <Input placeholder="标题名称"/>)}
                                </FormItem>
                            </Col>
                            <Col span={4}>
                                <FormItem label="频道"
                                          {...formItemLayout2}
                                >
                                    {getFieldDecorator("channel")(
                                        <Select notFoundContent="没有频道" placeholder="请选择频道" allowClear>
                                            {this.props.selection.channel ? this.handleSelection(this.props.selection.channel) : null}
                                        </Select>)}
                                </FormItem>
                            </Col>
                            <Col span={5}>
                                <FormItem label="初审状态"
                                          labelCol={{offset: 1, span: 8}}
                                          wrapperCol={{span: 14}}
                                >
                                    {getFieldDecorator("trialState")(
                                        <Select notFoundContent="没有内容" placeholder="请选择初审状态" allowClear>
                                            {this.props.selection.state ? this.handleSelection(this.props.selection.state) : null}
                                        </Select>)}
                                </FormItem>
                            </Col>
                            <Col span={3}>
                                <Button style={{marginTop: "4px"}} onClick={this.handleSearch}>查询</Button>
                            </Col>
                        </Row>
                    </Form>
                </div>
                <Table columns={columns}
                       dataSource={this.state.data.dataList}
                       onChange={this.handleTableChange}
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                       rowKey="id"
                />
            </div>
        )
    }
}

export default Form.create()(CheckListTemp);