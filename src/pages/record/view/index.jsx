import React from 'react';
import getContent from '../../../utils/content';
import Title from './title';
import CheckList from './checkList';
import ComponentHistory from './recordHistory';

export class RecordTemp extends React.Component {
    render() {
        return (
            <div>
                {
                    getContent(
                        <div className="content">
                            <Title {...this.props}/>
                        </div>
                    )}
                <br/>
                {
                    getContent(
                        <div className="content">
                            <CheckList {...this.props}/>
                        </div>
                    )
                }
            </div>
        );
    }
}

RecordTemp.defaultProps = {};

export class RecordHistoryTemp extends React.Component {

    render() {
        return (
            getContent(
                <div className="content">
                    <ComponentHistory params={this.props.match.params} {...this.props}/>
                </div>
            )
        )
    }
}

RecordHistoryTemp.defaultProps = {};