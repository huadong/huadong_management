import React from 'react';
import {Table} from 'antd';

const columns = [{
    title: '审核时间',
    dataIndex: 'createTime',
}, {
    title: '审核操作',
    dataIndex: 'operate',
    render: props => {
        return props == 0 ? "初审" : "复审"
    }
}, {
    title: '操作人',
    dataIndex: 'createUserName',
}, {
    title: '结果',
    dataIndex: 'result',
    render: props => {
        switch (props) {
            case "awaitTrial":
                return "待初审";
            case "trialPass":
                return "已通过";
            case "trialReject":
                return "已驳回";
            case "retrialPassWeight":
                return "已通过并加权";
            case "retrialPass":
                return "已通过";
            case "retrialReject":
                return "已驳回";
            case "retrialRejectDiscard":
                return "已驳回并废弃";
        }
    }
}, {
    title: '原因',
    dataIndex: 'reason',
}, {
    title: '通过时质量',
    dataIndex: 'passQuality',
    render: props => {
        return props == 0 ? "常规" : "高质量"
    }
}, {
    title: '额外操作',
    dataIndex: 'extraOperate',
}];

export default class RecordHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            pagination: {
                count: 1,
            },
            loading: false,
        }
    }

    componentDidMount() {
        this.props.getRecordHistory({id: this.props.params.id, page: 1});
    }

    componentWillReceiveProps(nextProps) {
        const {count} = this.state.pagination;
        if (nextProps) {
            this.setState({
                data: nextProps.history,
                pagination: {
                    pageSize: nextProps.history.pageSize,
                    total: nextProps.history.totalCount,
                    count: count,
                },
                loading: true,
            }, () => {
                this.setState({
                    loading: false,
                })
            })
        }
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
        })
        this.props.getRecordHistory({
            id: this.props.params.id,
            page: pagination.current,
        });
    }

    render() {
        console.log(this.props);
        return (
            <div>
                <Table columns={columns}
                       dataSource={this.state.data.dataList}
                       onChange={this.handleTableChange}
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                />
            </div>
        )
    }
}