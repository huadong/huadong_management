import React from 'react';
import {DatePicker, Input, Select, Button, Table, Form, Col, Divider, Row, LocaleProvider} from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
    labelCol: {span: 7},
    wrapperCol: {span: 10},
}

export default class Title extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: {},
        }
    }

    componentDidMount() {
        this.props.countData({});
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            this.setState({
                title: nextProps.title,
            })
        }
    }

    render() {
        console.log(this.props);
        return(
            <div>
                <Form layout="horizontal">
                    <h3>
                        今日审核概况
                    </h3>
                    <Row>
                        <Col span={6}>
                            <FormItem label="审核通过数"
                                      {...formItemLayout}
                            >
                                {
                                    this.state.title.passQty ? this.state.title.passQty : 0
                                }
                            </FormItem>
                        </Col>
                        <Col span={6}>
                            <FormItem label="审核驳回数"
                                      {...formItemLayout}
                            >
                                {
                                    this.state.title.rejectQty ? this.state.title.rejectQty : 0
                                }
                            </FormItem>
                        </Col>
                        <Col span={6}>
                            <FormItem label="审核汇总"
                                      {...formItemLayout}
                            >
                                {
                                    this.state.title.allQty ? this.state.title.allQty : 0
                                }
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}