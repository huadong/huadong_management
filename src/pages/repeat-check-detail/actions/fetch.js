import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return{
        type:actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}
export default function fetchData(id) {

    return (dispatch) => {
        dispatch(fetching());
        const token = getToken();

            req.getRequestDetail(token,config.getReCheckIdDetail+"?id="+id).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    let arr=[];
                    const moment = require('moment');
                    if(!!responseJson.data.articleDTO){
                        responseJson.data.articleDTO.sourceReleaseTime = moment(responseJson.data.articleDTO.sourceReleaseTime).format(config.TIME);
                        if(!!responseJson.data.articleDTO.cover){
                            responseJson.data.articleDTO.cover=responseJson.data.articleDTO.cover.split(',').slice(0,3);
                        }
                    }



                    dispatch(fetched(responseJson.data));
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    if(responseJson.retCode.code===-1001){
                        dispatch(fetched("已被审核"));
                    }else{
                        dispatch(fetchFail(responseJson.retCode.message));
                        msg(config.WARN, responseJson.retCode.message);
                    }
                }
            }).catch((err) => {
                dispatch(fetchFail(err));
                console.error(err);
            });

    }

}