/**
 * Created by Richie on 2018/4/12
 */
import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function outing() {
    return{
        type:actionTypes.OUT_REQUEST
    };
}

function outed(data) {
    return {
        type: actionTypes.OUT_SUCCESS,
        data,
    };
}

function outFail(error) {
    return {
        type: actionTypes.OUT_FAILURE,
        error,
    };
}
export default function out(id) {

    return (dispatch) => {
        dispatch(outing());

        const token = getToken();
        req.deleteRequestDetail(token,config.outOfCheck+id).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                dispatch(outed(responseJson.data));
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                if(responseJson.retCode.code ===-1000){
                    dispatch(outed(responseJson.data))
                }else{
                    dispatch(outFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }
        }).catch((err) => {
            dispatch(outFail(err));
            console.error(err);
        });
    }

}