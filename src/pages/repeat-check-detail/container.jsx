import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,checking,fetching ,checkContent,arr,outing,outData} = state.checkDetail.toJS();
    return {
        entityData,
        checking,
        fetching,
        checkContent,
        arr,
        outing,
        outData
    };
};

const mapDispatchToProps = dispatch => ({
    fetchData:(id) => {
        dispatch(actions.fetchData(id))
    },
    checkData:(data) => {
        dispatch(actions.checkData(data));
    },
    out:(id)=>{
        dispatch(actions.out(id));
    }
});

class Container extends Component {

    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);