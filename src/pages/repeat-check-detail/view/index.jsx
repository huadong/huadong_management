import React, {Component} from 'react';
import PropTypes from 'prop-types';

import getContent from '../../../utils/content';
import { Button,Card, Col, Row,Form,Input,Radio,Switch,Tag,Spin,Divider,notification} from 'antd';
import Tags from '../../../share-component/eidtor/tag';
import ShowImg from '../../../share-component/showImages';
import Selection from "../../../share-component/select";
import {withRouter} from 'react-router-dom';
import {getMap,getVal} from '../../../utils/map';
import HtmlBox from '../../../share-component/htmlBox';
import msg from '../../../utils/msg';
import config from '../../../config';
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;



class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state={
            //审核状态
            retrialState:"retrialPassWeight",
            articleQuality:"0",
            timeEffect:"1",
            commentState:"1",
            areaAttribute:"",
            reason:"",
            timeEffectStatus:false,
            commentStateStatus:false,
            areaAttributeStatus:false,
            channelStatus:false,


            isPhone:false,
            data:{
                repeat:0,
                repeatPercent:"58%"
            },
        }
    }

    componentDidMount() {
        const id= this.props.match.params.id;
        this.props.fetchData(id);
        this.setState({
            id:id
        });


        notification.destroy();
        if(this.state.data.repeat){
            notification['warning']({
                message: '温馨提示',
                description: '该篇文章线上有相似内容，重复度达'+this.state.data.repeatPercent,
                duration: 10,
            });
        }

    }

    componentWillUpdate(nextProps,nextState){
        if(this.props.checking!==nextProps.checking && nextProps.checking){
            if(!!nextProps.checkContent&&nextProps.checkContent.hasOwnProperty('id')){
                msg(config.SUCCESS,"审核成功，进入下一篇文章");
                this.props.history.push("/repeat-check-detail/"+nextProps.checkContent.id);
                this.props.fetchData(nextProps.checkContent.id);
            }else {
                msg(config.SUCCESS,"审核已完成，即将退出复审");
                setTimeout(()=>{
                    this.props.history.push("/repeat-check")
                },900)
            }

        }
        if(this.props.outing!==nextProps.outing && nextProps.outing){
            setTimeout(()=>{
                msg(config.LOAD,"退出复审");
                this.props.history.push("/repeat-check")
            },900)
        }
        if(this.props.fetching!==nextProps.fetching && nextProps.fetching && nextProps.entityData==="已被审核"){
            msg(config.LOAD,"当前文章已在审核中，即将退出");
            setTimeout(()=>this.props.history.push("/repeat-check"),1000)
        }
    }

    componentWillUnmount(){
        this.handleOutOfCheck();
    }

    handleOutOfCheck = ()=>{
        this.props.out(this.state.id)
    };
    handleRadioChange1 =(e) =>{
        e.preventDefault();
        this.setState({
            retrialState:e.target.value
        });
    };

    handleRadioChange2 =(e) =>{
        e.preventDefault();
        let status = e.target.value!==this.state.timeEffect;
        this.setState({
            timeEffect:e.target.value,
            timeEffectStatus:status
        });
    };
    handleRadioChange3 =(e) =>{
        e.preventDefault();
        let status = e.target.value!==this.state.commentState;
        this.setState({
            commentState:e.target.value,
            commentStateStatus:status,
        });
    };
    handleRadioChange4 =(e) =>{
        e.preventDefault();
        this.setState({
            articleQuality:e.target.value
        });
    };

    handleTogglePhoneOrPC =(e) =>{
        e.preventDefault();
        this.setState({
            isPhone:!this.state.isPhone
        })
    };

    handleCheck = (e) =>{
        e.preventDefault();
        const j ={};

        j.retrialState=this.state.retrialState;
        j.articleQuality=this.state.articleQuality;
        j.timeEffectStatus=this.state.timeEffectStatus;
        j.commentStateStatus=this.state.commentStateStatus;
        j.areaAttributeStatus=this.state.areaAttributeStatus;
        j.channelStatus=this.state.channelStatus;
        j.timeEffect=!!this.props.entityData.articleDTO.timeEffect?(this.state.timeEffectStatus?this.state.timeEffect:this.props.entityData.articleDTO.timeEffect):this.state.timeEffect;
        // j.areaAttribute=!!this.props.entityData.articleDTO.areaAttribute?(this.state.areaAttributeStatus?this.state.areaAttribute:this.props.entityData.articleDTO.areaAttribute):this.state.areaAttribute;
        j.commentState=!!this.props.entityData.articleDTO.commentState?(this.state.commentStateStatus?this.state.commentState:this.props.entityData.articleDTO.commentState):this.state.commentState;

        j.reason=this.state.reason;
        if(!!this.state.channel && this.state.channel !== this.props.entityData.articleDTO.channel){
            j.channel=this.state.channel;
        }
        j.title=this.props.entityData.articleDTO.title;
        j.id=this.props.entityData.articleDTO.id;
        if(!!this.state.tag &&  this.state.tag !== this.props.entityData.articleTags){
            j.articleTags=this.state.tag;
        }
        console.error(j);
        this.props.checkData(j);
    };

    handleChangeTags = (values) => {
        console.log(values);
        this.setState({
            tag:values
        })
    };

    handleSelectChange=(value) => {
        let status = (value !== this.props.entityData.articleDTO.channel);
        this.setState({
            channel:value,
            channelStatus:status
        })
    };

    handleInputChange=(e) => {
        e.preventDefault();
        let status = e.target.value!==this.state.areaAttribute;
        this.setState({
            areaAttribute:e.target.value,
            areaAttributeStatus:status
        });
    };

    handleAreaChange=(e) => {
        e.preventDefault();
        this.setState({
            reason:e.target.value
        })
    };

    render(){
        const {entityData,fetching,checking,checkData,checkContent} = this.props;
        const data = entityData.articleDTO;

        if(entityData.hasOwnProperty('articleDTO')&&entityData.hasOwnProperty('map')){
            return (
                <div>
                    <Form className="ant-advanced-search-form">
                        <Row gutter={16}>
                            <Col span={16}>
                                {getContent(
                                    <div className="content">
                                        <h3>基本信息</h3>
                                        <Divider/>
                                        <p>所属频道：{getVal(entityData.map.channel,data.channel)}</p>
                                        <Row gutter={16}>
                                            <Col span={8}>
                                                <span>投稿时间：{data.sourceReleaseTime}</span>
                                            </Col>
                                            <Col span={16}>
                                                <span>源地址：{data.sourceUrl}</span>
                                            </Col>
                                        </Row>
                                        <Row gutter={16}>
                                            <Col span={8}>
                                                <span>直接来源：{data.direct_source_Name}</span>
                                            </Col>
                                            <Col span={16}>
                                                <span>来　源：{data.sourceName}</span>
                                            </Col>
                                        </Row>
                                        <p>标　　题：{data.title}</p>
                                        <p>描　　述：{data.summary}</p>
                                        <div>
                                            封　　面：<ShowImg list={data.cover?data.cover:[]} style={{width:"100px",height:"80px",verticalAlign:"text-top",marginTop:"-12px",padding:"10px",cursor:"pointer"}}/>
                                        </div>
                                        {/*<FormItem label="标　　签:">*/}
                                            {/*<Tags tag={data.articleTags?data.articleTags:['Tag1']} onChange={(values)=>this.handleChangeTags(values)}/>*/}
                                        {/*</FormItem>*/}
                                    </div>
                                )}
                                <br/>
                                {getContent(
                                    <div className="content">
                                        <h3>初审信息</h3>
                                        <Divider/>
                                        <Row gutter={16}>
                                            <Col span={5}>
                                                <span>上次审核：<span style={{color:"#1E90FF"}}>{data.trialRecordDTO?getVal(entityData.map.auditStateMap,data.trialRecordDTO.trialState):""}</span></span>
                                            </Col>
                                            <Col span={5} offset={1}>
                                                <span>审核人：{data.trialRecordDTO?data.trialRecordDTO.trialUsername:""}</span>
                                            </Col>
                                            <Col span={8} offset={1}>
                                                <span>初审时间：{data.trialRecordDTO?data.trialRecordDTO.trialTime:""}</span>
                                            </Col>
                                        </Row>
                                    </div>
                                )}
                                <br/>
                                {getContent(
                                    <div className="content">
                                        <h3>
                                            文章内容
                                            <Button type="primary" onClick={this.handleTogglePhoneOrPC} style={{display:"inline-block",float:"right",margin:"0px",borderRadius:"15px"}} >{this.state.isPhone?"切换电脑视图预览":"切换手机视图预览"}</Button>
                                            {!this.state.data.repeat?(<span style={{display:"inline-block",float:"right",fontSize:"14px",fontWeight:"normal",color:"#444444",padding:"5px"}}>
                                          内容重复度：线上无相似内容
                                      </span>):(<span style={{display:"inline-block",float:"right",fontSize:"14px",fontWeight:"normal",color:"#ff0000",padding:"5px"}}>
                                          线上有相似内容，重复度达{this.state.data.repeatPercent}
                                      </span>)}
                                        </h3>
                                        <Divider/>
                                        <p>总计{data.content.length}字</p>
                                        <Row gutter={16}>
                                            <Col span={24}>
                                                <div style={{border:"1px solid \#999",borderRadius:"10px",padding:"10px"}}>
                                                    {this.state.isPhone?(
                                                        <Row gutter={16}>
                                                            <Col span={12} offset={6}>
                                                                <div style={{padding:"40px 20px 40px 20px",width:"340px",height:"600px",overflow:"auto",background:"#eee"}}>
                                                                    <HtmlBox isPhone={true} data={data.content}/>
                                                                </div>
                                                                <div style={{background:"#eee",height:"40px",width:"340px"}}>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    ):(<HtmlBox isPhone={false} data={data.content}/>)}
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                )}
                            </Col>
                            <Col span={8}>
                                {getContent(
                                    <div className="content">
                                        <h3>审核操作</h3>
                                        <Divider/>
                                        <FormItem label="审　　核：">
                                            <RadioGroup onChange={this.handleRadioChange1} value={this.state.retrialState} size="small">
                                                <RadioButton value="retrialPassWeight">通过并加权</RadioButton>
                                                <RadioButton value="retrialPass">通过</RadioButton>
                                                <RadioButton value="retrialReject">驳回</RadioButton>
                                                <RadioButton value="retrialRejectDiscard">驳回并废弃</RadioButton>
                                            </RadioGroup>
                                        </FormItem>
                                        <FormItem label="质　　量：">
                                            <RadioGroup onChange={this.handleRadioChange4} value={this.state.articleQuality}>
                                                <RadioButton value="0">常规</RadioButton>
                                                <RadioButton value="1">高质量</RadioButton>
                                            </RadioGroup>
                                        </FormItem>
                                        <FormItem label="时&nbsp;&nbsp;效&nbsp;&nbsp;性：">
                                            <RadioGroup onChange={this.handleRadioChange2} defaultValue={data.timeEffect?data.timeEffect:this.state.timeEffect}>
                                                <RadioButton value="1">无时效</RadioButton>
                                                <RadioButton value="2">1天</RadioButton>
                                                <RadioButton value="3">3天</RadioButton>
                                            </RadioGroup>
                                        </FormItem>
                                        <FormItem label="评　　论：">
                                            <RadioGroup onChange={this.handleRadioChange3} defaultValue={data.commentState?data.commentState:this.state.commentState}>
                                                <Radio value="1">正常评论</Radio>
                                                <Radio value="2">必审评论</Radio>
                                                <Radio value="3">隐藏评论</Radio>
                                                <Radio value="4">关闭评论</Radio>
                                            </RadioGroup>
                                        </FormItem>

                                        <FormItem label="更换频道(非必选)：">
                                            <Selection msg={getMap(entityData.map.channel)} dv={data.channel} onChange={(value)=>this.handleSelectChange(value)}/>
                                        </FormItem>

                                        <Row gutter={16}>
                                            <Col span={16}>
                                                <FormItem label="地区属性">
                                                    <Input placeholder="添加地区属性" disabled value={this.state.areaAttribute} onChange={this.handleInputChange}/>
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <FormItem label="原　　因：">
                                            <textarea style={{resize:"none",width:"80%",height:"60px",borderRadius:"5px",borderColor:"#999999"}} onChange={this.handleAreaChange} value={this.state.reason}/>
                                        </FormItem>

                                        <Divider dashed/>
                                        <Row gutter={16}>
                                            <Col span={16} style={{marginLeft:"20%"}}>
                                                <FormItem>
                                                    <Button type="primary" style={{width:"100%"}} onClick={this.handleCheck}>提交并进入下一条</Button>
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <Row gutter={16}>
                                            <Col span={16} style={{marginLeft:"20%"}}>
                                                <FormItem>
                                                    <Button type="default" style={{width:"100%"}} onClick={()=>this.props.history.push('/repeat-check')}>退出审核</Button>
                                                </FormItem>
                                            </Col>
                                        </Row>
                                    </div>
                                )}
                            </Col>
                        </Row>


                    </Form>

                </div>
            );
        }else {
            return <Spin size="large" />
        }

    }
}

AppComponent.propTypes = {
    entityData:PropTypes.object.isRequired,
    checkContent:PropTypes.object.isRequired,
    fetching:PropTypes.bool.isRequired,
    checking:PropTypes.bool.isRequired,
    checkData:PropTypes.func.isRequired,
    fetchData:PropTypes.func.isRequired,
    outing:PropTypes.bool.isRequired,
    out:PropTypes.func.isRequired,
};

const WrapAppComponent = withRouter(AppComponent);

export default WrapAppComponent;
