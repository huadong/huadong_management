import fetchData from './fetch';
import detailData from './detail';
import queryData from './query';

export {
    detailData,
    fetchData,
    queryData
};