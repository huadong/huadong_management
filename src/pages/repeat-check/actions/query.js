import actionTypes from '../actionTypes';
import { routerActions } from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import { getToken } from '../../../utils/auth';
import msg from '../../../utils/msg';


function querying() {
    return{
        type:actionTypes.QUERY_REQUEST
    };
}

function queried(data) {
    return {
        type: actionTypes.QUERY_SUCCESS,
        data,
    };
}

function queryFail(error) {
    return {
        type: actionTypes.QUERY_FAILURE,
        error,
    };
}

export default function queryData(query) {

    return (dispatch) => {
        dispatch(querying());
        const token = getToken();

            req.postRequestDetail(token,config.getReCheckRandomDetail,query).then((responseJson) => {

                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    console.error("aaa", responseJson.data);
                    if(!!responseJson.data){
                        msg(config.LOAD,"即将进入复审");
                    }

                    dispatch(queried(responseJson.data));
                }
                //业务逻辑错误
                if (responseJson.retCode.code < 0) {
                    dispatch(queryFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.retCode.message);
                }
            }).catch((err) => {
                dispatch(queryFail(err));
                console.error(err);
            });

    }

}