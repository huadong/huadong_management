import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

const mapStateToProps = (state) => {
    const { entityData,fetching,detailing,detailContent,queryContent,querying } = state.repeatCheck.toJS();
    return { entityData,fetching,detailing,detailContent,queryContent,querying };
};

const mapDispatchToProps = dispatch => ({
    fetchData:(page,query) => {
        dispatch(actions.fetchData(page,query))
    },
    detailData:() => {
        dispatch(actions.detailData());
    },
    queryData:(query)=>{
        dispatch(actions.queryData(query))
    }
});

class Container extends Component {


    render() {
        return (
            <ViewComponent {...this.props}/>
        )
    }
}

Container.propTypes = {
    fetchData:PropTypes.func.isRequired,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Container);