import actionTypes from '../actionTypes';

export default (state, action) => {
    switch (action.type) {
        case actionTypes.DETAIL_REQUEST:
            return state.merge({
                detailing:false,
            });
        case actionTypes.DETAIL_SUCCESS: {
            return state.merge({
                detailContent:action.data,
                detailing:true,
            });
        }
        case actionTypes.DETAIL_FAILURE:
            return state.merge({
                detailContent:{},
                detailing:false,
            });
        default:
            return state;
    }
};