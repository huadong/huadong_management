import { fromJS } from 'immutable';
import reduceReducers from 'reduce-reducers';

import fetchData from './fetch';
import detailData from './detail';
import queryData from './query';

const initialState = fromJS({
    entityData:{},
    fetching:false,

    detailing:false,
    detailContent:{},

    querying:false,
    queryContent:{}
});

const reducer = reduceReducers(
    (state = initialState, action) => fetchData(state, action),
    (state = initialState, action) => detailData(state, action),
    (state = initialState, action) => queryData(state, action),
);

export default reducer;