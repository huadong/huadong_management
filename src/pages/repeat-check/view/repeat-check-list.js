/**
 * Created by Richie on 2018/4/9
 */
import React, {Component} from 'react';
import moment from 'moment';
import {Link} from 'react-router-dom';
import {Form, Row, Col, Input, Button, DatePicker, Table} from 'antd';
import Selection from '../../../share-component/select';
import PushUrl from '../../../share-component/pushUrl';
import {getMap,getVal,getValArr} from '../../../utils/map';
import config from "../../../config";


class RepeatCheckList extends Component {
    constructor(props) {
        super(props);
        this.state={
            page:1,
            selectChannel:"",
            query:{}
        }
    }

    componentDidMount() {
        const {page,query} = this.state;
        this.props.fetchData(page,query);
    }

    componentWillUpdate(nextProps,nextState){
        if(this.state.page!==nextState.page){
            this.props.fetchData(nextState.page,{})
        }
        if(this.state.query!==nextState.query){
            const {query} = nextState;
            this.props.fetchData(1,query)
        }
    }


    handleChangePages = (page) =>{
        this.setState({
            page:page
        });
    };

    handleGetSelect = (value)=>{
        this.setState({
            selectChannel:value
        })
    };

    handleSearch = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            let q ={};
            if(!!values.date){
                q.sourceReleaseTime =moment(moment(values.date).format(config.DAY)).format(config.TIME)
            }
            if(Boolean(this.state.selectChannel)){
                q.channel = this.state.selectChannel;
            }
            if(!!values.title){
                q.title = values.title
            }
            if(!!values.trialUser){
                q.trialUser = values.trialUser
            }
            console.error("sendData",q);
            this.setState({
                query:q
            })
        });
    };

    render() {
        const {entityData,channelMap,sourceMap,checkMap} = this.props;
        const data = !!entityData.dataList?entityData.dataList:[];
        const ppp={defaultCurrent:entityData.page, total:entityData.totalCount, onChange:this.handleChangePages};
        const {getFieldDecorator}=this.props.form;
        const FormItem = Form.Item;
        const columns = [
            {
                title: '文章Id',
                dataIndex: 'id',
                className:'largeTable'
            }, {
                title: '标题',
                // dataIndex: 'title',
                className:'largeTable',
                render: props => {
                    return(
                        <a href={props.sourceUrl}>{props.title}</a>
                        )
                },
            }, {
                title: '频道',
                // dataIndex: 'channel',
                className:'largeTable',
                render:props=>{
                    return(
                        <span>
                            {getValArr(channelMap,props.channel)}
                        </span>
                    )
                }
            }, {
                title: '来源',
                // dataIndex: 'sourceType',
                className:'largeTable',
                render:props=>{
                    return(
                        <span>
                            {getValArr(sourceMap,props.sourceType)}
                        </span>
                    )
                }
            }, {
                title: '初审人',
                dataIndex: 'trialUser',
                className:'largeTable'
            }, {
                title: '初审结果',
                // dataIndex: 'trialState',
                className:'largeTable',
                render:props=>{
                    return(
                        <span>
                            {getValArr(checkMap,props.trialState)}
                        </span>
                    )
                }
            },  {
                title: '来源发布时间',
                dataIndex: 'sourceReleaseTime',
                className:'largeTable'
            }, {
                title: '操作',
                className:'largeTable',
                render: props => {
                    return (
                        <div>
                            <PushUrl
                                title="进入审核" size="small" style={{fontSize:'12px'}} id={props.id} path="/repeat-check-detail" type="default"
                            />
                            &nbsp;
                        </div>
                    )
                }
            }
        ];
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User', // Column configuration not to be checked
                name: record.name,
            }),
        };
        return (
            <div>
                <Form
                    className="ant-advanced-search-form"
                    onSubmit={this.handleSearch}
                >
                    <Row gutter={16} style={{display:"flex",alignItem:"center"}}>
                        <Col span={5}>
                            <FormItem label="日期">
                                {getFieldDecorator('date')(
                                    <DatePicker/>
                                )}
                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="文章标题">
                                {getFieldDecorator('title')(
                                    <Input placeholder="文章标题" />
                                )}

                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="编辑姓名">
                                {getFieldDecorator('trialUser')(
                                    <Input placeholder="输入编辑姓名" />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="频道">
                                <Selection msg={channelMap} dv="" onChange={(value)=>this.handleGetSelect(value)}/>
                            </FormItem>
                        </Col>
                        <Col span={3} style={{ textAlign: 'left' }}>
                            <FormItem>
                                <Button type="primary" htmlType="submit"  icon="search">查询</Button>
                            </FormItem>

                        </Col>
                    </Row>
                    <Table rowSelection={rowSelection} dataSource={data} rowKey="id" pagination={ppp} columns={columns} loading={!(!!entityData.dataList)}>
                    </Table>
                </Form>
            </div>
        )
    }
}

export default Form.create()(RepeatCheckList);

