import keyMirror from 'keymirror';

export default keyMirror({
    FETCH_REQUEST: null,
    FETCH_SUCCESS: null,
    FETCH_FAILURE: null,

    ADD_REQUEST: null,
    ADD_SUCCESS: null,
    ADD_FAILURE: null,

    GETSELECTION_REQUEST: null,
    GETSELECTION_SUCCESS: null,
    GETSELECTION_FAILURE: null,

    ACTION_REQUEST: null,
    ACTION_SUCCESS: null,
    ACTION_FAILURE: null,
})