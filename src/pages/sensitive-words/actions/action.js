import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function actioning() {
    return {
        type: actionTypes.ACTION_REQUEST
    };
}

function actioned(data, resData) {
    return {
        type: actionTypes.ACTION_SUCCESS,
        message: data,
        res: resData
    };
}

function actionFail(error) {
    return {
        type: actionTypes.ACTION_FAILURE,
        error,
    };
}

export default function actionData(action, params) {

    return (dispatch) => {
        dispatch(actioning());
        const token = getToken();
        let url = "";
        if (action == "disabled")
            url = config.Sensitive.disableSensitiveWords + `${params.id}?version=${params.version}`;
        if (action == "batchDelete")
            url = config.Sensitive.batchDelete;
        if (action == "modify")
            url = config.Sensitive.modifySensitiveWords;
        if (action == "enable")
            url = config.Sensitive.enableSensitiveWords + `${params.id}?version=${params.version}`;
        if (action == "batchEnable")
            url = config.Sensitive.batchEnableSensitiveWords;
        if (action == "batchDisable")
            url = config.Sensitive.batchDisableSensitiveWords;
        if (action == "batchModify")
            url = config.Sensitive.batchModifySensitiveWords;
        if (action == "delete") {
            url = config.Sensitive.deleteSensitiveWords + `${params.id}?version=${params.version}`;
            req.deleteRequestDetail(token, url, {}).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    // console.warn("responseJson", responseJson.data);
                    dispatch(actioned("delete"))
                }
                //业务逻辑错误
                else if (responseJson.retCode.code < 0) {
                    dispatch(actionFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.message);
                }
                else {
                    //用于格式化时间
                    const moment = require('moment');
                    // console.warn("responseJson", responseJson.data);
                    dispatch(actionFail(responseJson.retCode.message))
                }
            }).catch((err) => {
                dispatch(actionFail(err));
                console.error(err);
            });
        } else {
            req.postRequestDetail(token, url, params).then((responseJson) => {
                if (responseJson.retCode.code === 200) {
                    //用于格式化时间
                    const moment = require('moment');
                    // console.warn("responseJson", responseJson.data);
                    dispatch(actioned(action, responseJson.data));
                }
                //业务逻辑错误
                else if (responseJson.retCode.code < 0) {
                    dispatch(actionFail(responseJson.retCode.message));
                    msg(config.WARN, responseJson.message);
                }
                else {
                    //用于格式化时间
                    const moment = require('moment');
                    // console.warn("responseJson", responseJson.data);
                    dispatch(actionFail(responseJson.retCode.message))
                }
            }).catch((err) => {
                dispatch(actionFail(err));
                console.error(err);
            });
        }
    }
}