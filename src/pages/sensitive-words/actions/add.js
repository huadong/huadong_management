import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken} from '../../../utils/auth';
import msg from '../../../utils/msg';


function adding() {
    return {
        type: actionTypes.ADD_REQUEST
    };
}

function added(data) {
    return {
        type: actionTypes.ADD_SUCCESS,
    };
}

function addFail(error) {
    return {
        type: actionTypes.ADD_FAILURE,
        error,
    };
}

const data = [
    {
        key: "1",
        words: "1",
        range: "2",
        updateTime: "4",
        status: "3",
    }
];

export default function addData(params) {

    return (dispatch) => {
        dispatch(adding());
        const token = getToken();

        req.postRequestDetail(token, config.Sensitive.addSensitiveWords, params).then((responseJson) => {
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                dispatch(added())
            }
            //业务逻辑错误
            else if (responseJson.retCode.code < 0) {
                dispatch(addFail(responseJson.retCode.message));
                msg(config.WARN, responseJson.message);
            }
            else {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                dispatch(addFail(responseJson.retCode.message))
            }
        }).catch((err) => {
            dispatch(addFail(err));
            console.error(err);
        });
    }
}