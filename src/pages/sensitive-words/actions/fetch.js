import actionTypes from '../actionTypes';
import {routerActions} from 'react-router-redux';
import * as req from '../../../utils/request';
import config from '../../../config';
import {getToken, setStorage} from '../../../utils/auth';
import msg from '../../../utils/msg';


function fetching() {
    return {
        type: actionTypes.FETCH_REQUEST
    };
}

function fetched(data) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        data,
    };
}

function fetchFail(error) {
    return {
        type: actionTypes.FETCH_FAILURE,
        error,
    };
}

const data = [
    {
        key: "1",
        words: "1",
        range: "2",
        updateTime: "4",
        status: "3",
    }
];


export default function fetchData(params) {

    return (dispatch) => {
        dispatch(fetching());
        const token = getToken();
        console.log(config.Sensitive.querySensitiveWords);
        req.postRequestDetail(token, config.Sensitive.querySensitiveWords, params).then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.retCode.code === 200) {
                //用于格式化时间
                const moment = require('moment');
                console.warn("responseJson", responseJson.data);
                if (responseJson.data && responseJson.data.records.length !== 0) {

                    dispatch(fetched(responseJson.data));
                } else {
                    dispatch(fetched({}))
                }
            }
            //业务逻辑错误
            if (responseJson.retCode.code < 0) {
                dispatch(fetchFail(responseJson.message));
                msg(config.WARN, responseJson.message);
            }
        }).catch((err) => {
            dispatch(fetchFail(err));
            console.error(err);
        });
    }
}