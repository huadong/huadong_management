import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ViewComponent from './view';
import * as actions from './actions';

class Container extends Component {
    componentDidMount(){
        this.props.fetchData();
    }

    render() {
        return(
            <ViewComponent {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.sensitive.data,
        addContent: state.sensitive.addContent,
        actionContent: state.sensitive.actionContent,
        selection: state.sensitive.selection,
        fetching: state.sensitive.fetching,
        adding: state.sensitive.adding,
        getting: state.sensitive.getting,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (params) => {
            dispatch(actions.fetchData(params));
        },
        addData: (params) => {
          dispatch(actions.addData(params));
        },
        getSelection: () => {
            dispatch(actions.getSelection())
        },
        actions: (action, params) => {
            dispatch(actions.actions(action, params))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);