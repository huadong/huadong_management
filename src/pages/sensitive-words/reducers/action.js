import actionTypes from '../actionTypes';

export default (state, action) => {

    switch (action.type) {
        case actionTypes.ACTION_REQUEST:
            return {
                ...state,
                actioned: false,
                actionContent: {
                    message: ""
                }
            }
        case actionTypes.ACTION_SUCCESS:
            return {
                ...state,
                actioned: true,
                actionContent: {
                    message: action.message,
                    res: action.res ? action.res : {},
                }
            }
        case actionTypes.ACTION_FAILURE:
            return {
                ...state,
                actionContent: {
                    message: action.error
                }
            }
        default:
            return state;
    }

}