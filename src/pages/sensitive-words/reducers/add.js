import actionTypes from '../actionTypes';

export default (state, action) => {

    switch (action.type) {
        case actionTypes.ADD_REQUEST:
            return {
                ...state,
                added: false,
            }
        case actionTypes.ADD_SUCCESS:
            return {
                ...state,
                added: true,
                addContent: {
                    message: "success"
                }
            }
        case actionTypes.ADD_FAILURE:
            return {
                ...state,
                addContent: {
                    message: action.error
                }
            }
        default:
            return state;
    }

}