import fectchData from './fetch';
import addData from './add';
import getSelection from './getSelection';
import actions from './action';
import reduceReducers from 'reduce-reducers';

/*
初始化state
 */

const initialState = {
    data: {},
    addContent: {},
    actionContent: {},
    selection: {},
    fetching: false,
    adding: false,
    getting: false
};

/*
reducer
 */

const reducer = reduceReducers(
    (state = initialState, action) => fectchData(state, action),
    (state = initialState, action) => addData(state, action),
    (state = initialState, action) => getSelection(state, action),
    (state = initialState, action) => actions(state, action),
);


export default reducer
