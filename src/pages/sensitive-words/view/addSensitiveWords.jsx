import React from 'react';
import {Button, Select, Form, Col, Row, Input, Table, Modal} from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;
const {TextArea} = Input;

const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 14},
};
const formItemLayout2 = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

class AddSensitiveWords extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selection: {},
        }
    }

    componentDidMount() {
        console.log(this.props);
        this.props.getSelection()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selection: nextProps.selection,
        })
    }

    componentWillUnmount(){
        this.state = {
            selection: {},
        }
        this.props.form.setFieldsValue({
            name: "",
            type: "",
            state: "",
            scope: "",
        })
    }

    handleSelection = (item) => {
        const array = [];
        for (let o in item) {
            array.push(
            <Option value={o} key={o}>
                {item[o]}
            </Option>)
        }
        return array;
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div style={{height: "200px"}}>
                <Form layout="horizontal">
                    <Row>
                        <Col span={20}>
                            <FormItem label="输入你想添加的敏感词"
                                      labelCol={{span: 24}}
                                      wrapperCol={{span: 24}}
                                      style={{marginLeft: "30px"}}
                            >
                                {getFieldDecorator("name", {
                                    rules: [
                                        {required: true, message: "敏感词是必填项！"}
                                    ]
                                })(
                                    <TextArea autosize={{minRows: 4, maxRows: 4}} style={{width: "250px"}}/>)}
                            </FormItem>
                        </Col>
                        <div style={{position: "absolute", marginLeft: "300px", marginTop: "40px", fontSize: "12px"}}>
                            可以每行一个添加多个敏感词，<br/>
                            并支持以"&&"为连接的复合词<br/>
                            （在检测项中同时出现的多个<br/>
                            词，通常复合词仅为高亮警示级别）
                        </div>
                    </Row>
                    <Row>
                        <Col span={5}>
                            <FormItem label="级别"
                                      labelCol={{span: 9}}
                                      wrapperCol={{span: 14}}
                            >
                                {getFieldDecorator("level", {
                                    rules: [
                                        {required: true, message: "级别是必填项！"}
                                    ]
                                })(
                                    <Select getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.state.selection.level ? this.handleSelection(this.state.selection.level) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="分类"
                                      labelCol={{span: 9}}
                                      wrapperCol={{span: 14}}
                            >
                                {getFieldDecorator("type", {
                                    rules: [
                                        {required: true, message: "分类是必填项！"}
                                    ]
                                })(
                                    <Select getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.state.selection.type ? this.handleSelection(this.state.selection.type) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={9}>
                            <FormItem label="应用范围"
                                      labelCol={{span: 10}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("scope", {
                                    rules: [
                                        {required: true, message: "应用范围是必填项！"}
                                    ]
                                })(
                                    <Select getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.state.selection.scope ? this.handleSelection(this.state.selection.scope) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="状态"
                                      labelCol={{span: 9}}
                                      wrapperCol={{span: 14}}
                            >
                                {getFieldDecorator("state", {
                                    rules: [
                                        {required: true, message: "状态是必填项！"}
                                    ]
                                })(
                                    <Select getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.state.selection.state ? this.handleSelection(this.state.selection.state) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default Form.create()(AddSensitiveWords);