import React from 'react';
import Component from './sensitiveWords';
import getContent from '../../../utils/content';


export default class AppComponent extends React.Component {


    render() {

        return(
            getContent(
                <div className="content">
                    <Component {...this.props}/>
                </div>)
        )
    }
}