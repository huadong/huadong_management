import React from 'react';
import {Select, Form, Col, Row} from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;


class MultiSetTemp extends React.Component {

    handleSelection = (item) => {
        const array = [];
        for (let o in item) {
            array.push(
                <Option value={o} key={o}>
                    {item[o]}
                </Option>)
        }
        return array;
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div style={{height: "100px", width: "600px"}}>
                <Form layout="horizontal">
                    <Row>
                        <span>
                            将选中的敏感词，批量设置为：
                        </span>
                    </Row>
                    <Row>
                        <Col span={7}>
                            <FormItem label="级别"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                                      style={{marginBottom: "5px"}}
                            >
                                {getFieldDecorator("level")(
                                    <Select allowClear
                                            onChange={this.handleData}
                                            placeholder="级别"
                                            getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.props.selection.level ? this.handleSelection(this.props.selection.level) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={7}>
                            <FormItem label="分类"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("type")(
                                    <Select allowClear
                                            onChange={this.handleData}
                                            placeholder="分类"
                                            getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.props.selection.type ? this.handleSelection(this.props.selection.type) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={10}>
                            <FormItem label="应用范围"
                                      labelCol={{span: 8}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("scope")(
                                    <Select allowClear
                                            onChange={this.handleData}
                                            placeholder="应用范围"
                                            getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.props.selection.scope ? this.handleSelection(this.props.selection.scope) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={7}>
                            <FormItem label="状态"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("state")(
                                    <Select allowClear
                                            onChange={this.handleData}
                                            placeholder="状态"
                                            getPopupContainer={triggerNode => triggerNode.parentNode}>
                                        {this.props.selection.state ? this.handleSelection(this.props.selection.state) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

const MultiSet = Form.create()(MultiSetTemp);

class SetTemp extends React.Component {

    handleSelection = (item) => {
        const array = [];
        for (let o in item) {
            array.push(
                <Option value={o} key={o}>
                    {item[o]}
                </Option>)
        }
        return array;
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        this.props.handleInfo(this.props.form.getFieldsValue());
        return (
            <div style={{height: "100px", width: "600px"}}>
                <Form layout="horizontal">
                    <Row>
                        <Col span={7}>
                            <FormItem label="级别"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                                      style={{marginBottom: "5px"}}
                            >
                                {getFieldDecorator("level")(
                                    <Select onChange={this.handleData} allowClear placeholder="级别">
                                        {this.props.selection.level ? this.handleSelection(this.props.selection.level) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={7}>
                            <FormItem label="分类"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("type")(
                                    <Select onChange={this.handleData} allowClear placeholder="分类">
                                        {this.props.selection.type ? this.handleSelection(this.props.selection.type) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={10}>
                            <FormItem label="应用范围"
                                      labelCol={{span: 8}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("scope")(
                                    <Select onChange={this.handleData} allowClear placeholder="应用范围">
                                        {this.props.selection.scope ? this.handleSelection(this.props.selection.scope) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={7}>
                            <FormItem label="状态"
                                      labelCol={{span: 6}}
                                      wrapperCol={{span: 12}}
                            >
                                {getFieldDecorator("state")(
                                    <Select onChange={this.handleData} allowClear placeholder="状态">
                                        {this.props.selection.state ? this.handleSelection(this.props.selection.state) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

const Set = Form.create()(SetTemp);

export {
    MultiSet,
    Set
}

