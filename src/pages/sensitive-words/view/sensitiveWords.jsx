import React from 'react';
import {Button, Select, Form, Col, Row, Input, Table, Modal} from 'antd';
import AddSensitiveWords from './addSensitiveWords';
import {MultiSet, Set} from './multiSet';
import MultiButton from '../../../share-component/multi-button';

const Option = Select.Option;
const FormItem = Form.Item;
const {TextArea} = Input;

const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 14},
};
const formItemLayout2 = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

let params;

class sensitiveWords extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            message: "",
            selectedList: [],
            modifyData: [],
            pagination: {
                count: 1,
            },
            loading: false,
            params: {},
        }
    }

    shouldChange = (par) => {
        const {params} = this.state;
        for (let o in par) {
            if (par[o] != params[o]) {
                this.setState({
                    params: par,
                })
                return false;
            }
        }
        return true;
    }

    componentWillReceiveProps(nextProps) {
        const {count} = this.state.pagination;
        console.log(nextProps);
        if (nextProps) {
            if (this.shouldChange(nextProps.form.getFieldsValue())) {
                this.setState({
                    data: {
                        list: nextProps.data.records,
                        message: nextProps.addContent.message,
                    },
                    pagination: {
                        pageSize: nextProps.data.pageSize,
                        total: nextProps.data.totalCount,
                        count: count,
                    },
                    loading: true,
                }, () => {
                    this.setState({
                        loading: false,
                    })
                    if (nextProps.addContent.message && nextProps.addContent.message != "") {
                        if (nextProps.addContent.message == "success") {
                            this.refs.addWords.setFieldsValue({
                                name: "",
                                type: "",
                                state: "",
                                scope: "",
                            });
                            Modal.success({
                                title: "添加成功"
                            })
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else {
                            Modal.error({
                                title: this.props.addContent.message
                            })
                        }
                    }
                    if (nextProps.actionContent.message && nextProps.actionContent.message != "") {
                        if (nextProps.actionContent.message == "delete") {
                            Modal.success({
                                title: "删除成功",
                            })
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else if (nextProps.actionContent.message == "modify") {
                            Modal.success({
                                title: "修改成功",
                            })
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else if (nextProps.actionContent.message == "disabled") {
                            Modal.success({
                                title: "禁用成功",
                            })
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else if (nextProps.actionContent.message == "enable") {
                            Modal.success({
                                title: "启用成功",
                            })
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else if (nextProps.actionContent.message == "batchEnable") {
                            if (nextProps.actionContent.res.fail == 0) {
                                Modal.success({
                                    title: "批量启用成功",
                                })
                            }
                            else {
                                Modal.error({
                                    title: "批量启用有" + nextProps.actionContent.res.fail + "条失败",
                                })
                            }
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });

                        }
                        else if (nextProps.actionContent.message == "batchDisable") {
                            if (nextProps.actionContent.res.fail == 0) {
                                Modal.success({
                                    title: "批量禁用成功",
                                })
                            }
                            else {
                                Modal.error({
                                    title: "批量禁用有" + nextProps.actionContent.res.fail + "条失败",
                                })
                            }
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else if (nextProps.actionContent.message == "batchModify") {
                            if (nextProps.actionContent.res.fail == 0) {
                                Modal.success({
                                    title: "批量修改成功",
                                })
                            }
                            else {
                                Modal.error({
                                    title: "批量修改有" + nextProps.actionContent.res.fail + "条失败",
                                })
                            }
                            this.props.fetchData({
                                page: this.state.pagination.count,
                            });
                        }
                        else {
                            Modal.error({
                                title: this.props.actionContent.message
                            })
                        }
                    }
                })
            }
        }
    }

    componentDidMount() {
        const params = {};
        this.props.fetchData(params);
        this.props.getSelection();
    }

    componentWillUpdate(nextProps) {

    }

    handleOk = () => {
        this.refs.addWords.validateFields((err, values) => {
            if (!err) {
                this.props.addData(values);
            }
        });
    }

    handleSelection = (item) => {
        const array = [];
        for (let o in item) {
            array.push(
                <Option value={o} key={o}>
                    {item[o]}
                </Option>)
        }
        return array;
    }

    handleSearch = () => {
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        this.props.fetchData(params);
    }

    handleInfo = (param) => {
        for (let o in param) {
            if (!param[o])
                delete param[o];
        }
        params = param;
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.count = pagination.current;
        this.setState({
            pagination: pager,
            // selectedList: [],
        })
        const params = this.props.form.getFieldsValue();
        for (let o in params) {
            if (!params[o])
                delete params[o];
        }
        this.props.fetchData({
            ...params,
            page: pagination.current,
        });
    }

    render() {
        const columns = [{
            title: '敏感词',
            dataIndex: 'name',
            width: "200px"
        }, {
            title: '应用范围',
            dataIndex: 'scope',
            width: "200px"
        }, {
            title: '更新时间',
            dataIndex: 'lastModifiedTime',
            width: "200px",
        }, {
            title: '状态',
            dataIndex: 'state',
            width: "200px",
        }, {
            title: '操作',
            render: props => {
                return (
                    <div>
                        <MultiButton
                            title="设置"
                            type="primary"
                            modalTitle="敏感词设置"
                            modalText="确定要将选中的敏感词都进行启用吗"
                            select={true}
                            width="600px"
                            handleOk={() => {
                                console.log({id: props.id, ...params});
                                this.props.actions("modify", {id: props.id, version: props.version, ...params});
                            }}
                            size="small"
                        >
                            <Set selection={this.props.selection}
                                 handleInfo={this.handleInfo}
                            />
                        </MultiButton>
                        &nbsp;&nbsp;
                        <MultiButton
                            title={props.state == "启用" ? "禁用" : "启用"}
                            type="primary"
                            modalTitle={props.state == "启用" ? "敏感词禁用" : "敏感词启用"}
                            modalText={props.state == "启用" ? `确定要将选中的敏感词都进行禁用吗` : "确定要将选中的敏感词都进行启用吗"}
                            id={props.id}
                            size="small"
                            handleOk={() => {
                                props.state == "启用" ? this.props.actions("disabled", {
                                        id: props.id,
                                        version: props.version
                                    }) :
                                    this.props.actions("enable", {id: props.id, version: props.version});
                            }}
                        />&nbsp;&nbsp;
                        <MultiButton
                            title="删除"
                            type="primary"
                            modalTitle="敏感词删除"
                            modalText="确定要将这个词进行删除吗？"
                            id={props.id}
                            size="small"
                            handleOk={() => {
                                this.props.actions("delete", {id: props.id, version: props.version})
                            }}
                        />
                    </div>
                )
            }
        }];
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(selectedRows);
                this.setState({
                    selectedList: selectedRows,
                })
            },
        }
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Form layout="horizontal"
                >
                    <Row>
                        <Col span={4}>
                            <FormItem label="级别"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator('level', {})(
                                    <Select allowClear={true} placeholder="请选择级别">
                                        {this.props.selection.level ? this.handleSelection(this.props.selection.level) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="分类"
                                      {...formItemLayout}
                            >
                                {getFieldDecorator('type', {})(
                                    <Select allowClear={true} placeholder="请选择分类">
                                        {this.props.selection.type ? this.handleSelection(this.props.selection.type) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={5}>
                            <FormItem label="应用范围"
                                      labelCol={{span: 8}}
                                      wrapperCol={{span: 16}}
                            >
                                {getFieldDecorator('scope', {})(
                                    <Select allowClear={true} placeholder="请选择应用范围">
                                        {this.props.selection.scope ? this.handleSelection(this.props.selection.scope) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label="状态"
                                      labelCol={{span: 6, offset: 3}}
                                      wrapperCol={{span: 14}}
                            >
                                {getFieldDecorator('state', {})(
                                    <Select allowClear={true} placeholder="请选择状态">
                                        {this.props.selection.state ? this.handleSelection(this.props.selection.state) : null}
                                    </Select>)}
                            </FormItem>
                        </Col>
                        <Col span={3}>
                            <FormItem wrapperCol={{span: 17, offset: 3}}
                            >
                                {getFieldDecorator('nameLike', {})(
                                    <Input placeholder="请输入关键字"/>)}
                            </FormItem>
                        </Col>
                        <Col>
                            <Button onClick={this.handleSearch} style={{height: '32px', marginTop: '4px'}}
                                    type="primary">查询</Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <MultiButton style={{height: '32px', marginTop: '4px'}}
                                         title="添加敏感词"
                                         modalTitle="添加敏感词"
                                         select={true}
                                         handleOk={this.handleOk}
                                         width="700px"
                                         ref="multiButton"
                            >
                                <AddSensitiveWords ref="addWords"
                                                   getSelection={this.props.getSelection}
                                                   selection={this.props.selection}
                                />
                            </MultiButton>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={9}>
                            <FormItem
                                label="批量操作"
                                labelCol={{span: 4}}
                                wrapperCol={{span: 20}}
                            >
                                <div>
                                    <MultiButton
                                        title="批量设置"
                                        type="primary"
                                        modalTitle="批量设置"
                                        select={true}
                                        width='600px'
                                        handleOk={() => {
                                            const param = this.refs.multiSet.getFieldsValue();
                                            for (let o in param) {
                                                if (!param[o])
                                                    delete param[o]
                                            }
                                            console.log(param);
                                            const params = [];
                                            const {selectedList} = this.state;
                                            selectedList.map(item => {
                                                params.push({
                                                    id: item.id,
                                                    version: item.version,
                                                    ...param,
                                                })
                                            })
                                            console.log(params);
                                            this.props.actions("batchModify", params);
                                        }}
                                    >
                                        <MultiSet ref="multiSet"
                                                  selection={this.props.selection}
                                        />
                                    </MultiButton>&nbsp;&nbsp;
                                    <MultiButton
                                        title="批量启用"
                                        type="primary"
                                        modalTitle="批量设置"
                                        modalText="确定要将选中的敏感词都进行启用吗？"
                                        handleOk={() => {
                                            const params = [];
                                            const {selectedList} = this.state;
                                            selectedList.map(item => {
                                                params.push({
                                                    id: item.id,
                                                    version: item.version,
                                                })
                                            })
                                            this.props.actions("batchEnable", {lines: params});
                                        }}
                                    />&nbsp;&nbsp;
                                    <MultiButton
                                        title="批量禁用"
                                        type="primary"
                                        modalTitle="批量设置"
                                        modalText="确定要将选中的敏感词都进行禁用吗？"
                                        handleOk={() => {
                                            const params = [];
                                            const {selectedList} = this.state;
                                            selectedList.map(item => {
                                                params.push({
                                                    id: item.id,
                                                    version: item.version,
                                                })
                                            })
                                            this.props.actions("batchDisable", {lines: params});
                                        }}
                                    />
                                </div>
                            </FormItem>
                        </Col>
                    </Row>
                    <Table onChange={this.handleTableChange}
                           pagination={this.state.pagination}
                           loading={this.state.loading}
                           rowSelection={rowSelection}
                           columns={columns}
                           dataSource={this.state.data.list}
                           rowKey="id"
                    />
                </Form>
            </div>
        );
    }
}

const AppComponent = Form.create()(sensitiveWords);

AppComponent.defaultProps = {};
export default AppComponent;
