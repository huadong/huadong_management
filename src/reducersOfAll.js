/**
 * Created by Richie on 2018/4/12
 */

import content from './pages/content/reducers';
import addArticle from './pages/add-article/reducers';
import sensitive from './pages/sensitive-words/reducers';
import record from './pages/record/reducers';
import personalCenter from './pages/personalCenter/reducers';
import charMana from './pages/charactorManagement/reducers';
import memMana from './pages/memberManagement/reducers';
import memCheck from './pages/member/reducers';
import login from './pages/login/reducers';

import editArticle from './pages/edit-article/reducers';
import firstCheck from './pages/first-check/reducers';
import repeatCheck from './pages/repeat-check/reducers';
import checkDetail from './pages/check-detail/reducers';
import repeatCheckDetail from './pages/repeat-check-detail/reducers';

export default {
    content,
    addArticle,
    sensitive,
    editArticle,
    firstCheck,
    repeatCheck,
    checkDetail,
    repeatCheckDetail,
    record,
    personalCenter,
    charMana,
    memMana,
    memCheck,
    login,
}