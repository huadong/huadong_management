import React from "react";
import {BrowserRouter as Router, Route, Redirect, Switch} from "react-router-dom";
import './static/index.css';
import {LocaleProvider} from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import 'moment/locale/zh-cn';
import LayoutComponent from './share-component/layout';

import Home from './pages/content/container';
import FirstCheck from './pages/first-check/container';
import RepeatCheck from './pages/repeat-check/container';
import AddArticle from './pages/add-article/container';
import EditArticle from './pages/edit-article/container';
import CheckDetail from './pages/check-detail/container';
import RepeatCheckDetail from './pages/repeat-check-detail/container';

import Login from './pages/login/container';
import {Member, MemberCheckHistory} from './pages/member/container';
import {Record, RecordHistory} from './pages/record/container';
import SensitiveWords from './pages/sensitive-words/container';
import PersonalCenter from './pages/personalCenter/container';
import MemberManagement from './pages/memberManagement/container';
import CharactorManagement from './pages/charactorManagement/container';

import { isLoggedIn } from '../src/utils/auth';



const routes = [
    {
        path: '/first-check',
        component: FirstCheck,
    }, {
        path: '/add-article',
        component: AddArticle
    }, {
        path: '/edit-article',
        component: EditArticle
    }, {
        path: '/member',
        component: Member
    }, {
        path: '/record',
        component: Record
    }, {
        path: '/check-detail/:id',
        component: CheckDetail
    }, {
        path: '/repeat-check-detail/:id',
        component: RepeatCheckDetail
    },
    {
        path: '/repeat-check',
        component: RepeatCheck,
    }, {
        path: '/sensitive-words',
        component: SensitiveWords
    }, {
        path: '/record-history/:id',
        component: RecordHistory
    }, {
        path: '/personal-center/:id',
        component: PersonalCenter
    }, {
        path: '/mem-mana',
        component: MemberManagement
    }, {
        path: '/char-mana',
        component: CharactorManagement
    }, {
        path: '/record-member-history/:id',
        component: MemberCheckHistory
    }
];

const RouteWithSubRoutes = (route) => (
    <Route exact path={route.path} render={
        (props) => !isLoggedIn() ? (<Redirect to="/login"/>) : (<LayoutComponent>
            <route.component {...props} routes={route.routes}/>
        </LayoutComponent>)
    }/>
);

// eslint-disable-next-line
const PageNotFound = () => (
    <div className="content" style={{"textAlign":"center"}}>
        <h1>404</h1>
        <h2>页面不存在</h2>
        <p>
            <a href="/">回到首页</a>
        </p>
    </div>
);

const Rou = () => (
    <Router>
        <Switch>
            <RouteWithSubRoutes exact path="/" component={Home}/>
            {routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route}/>
            ))}
            <Route exact path="/login" component={Login}/>
            <Route component={PageNotFound}/>
        </Switch>
    </Router>
)

const Local =  () => (
    <LocaleProvider locale={zh_CN}>
        <Rou/>
    </LocaleProvider>
);

export default Local;

