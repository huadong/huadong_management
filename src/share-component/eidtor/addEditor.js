/**
 * Created by Richie on 2018/4/10
 */
import React, {Component} from 'react';
import { Form, Row, Col, Input, Button ,DatePicker,Divider,Radio,Upload, Icon, Modal} from 'antd';
import Editor from './wangEditor';
import Selection from '../select';
import Tags from './tag';
import defaultImage from '../../static/default.png';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;


class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state={

            //单选值
            radioValue:1,

            //图片预览
            previewVisible: false,
            previewImage: '',

            //上传图片列表
            fileList: [{
                uid: -1,
                name: 'default.png',
                status: 'done',
                url: defaultImage,
            }],

            //富文本内容
            data:''
        }

    }


    //取消图片预览
    handleCancel = () => this.setState({ previewVisible: false })

    //图片预览
    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    };

    //封面图片上传
    handleChange = ({ fileList }) => {
        fileList.forEach((item,index)=>{
            if(item.uid ===-1 && item.status==="done"){
                fileList.splice(index,1)
            }
        });
        this.setState({ fileList })
    };

    //单选
    handleRadioChange =(e) =>{
        e.preventDefault();
        this.setState({
            radioValue: e.target.value,
        });
    };



    //form提交
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log('Received values of form: ', values);
        });

    };

    render() {
        const getFieldDecorator = this.props.getField;
        const msg = [{key:1,val:'jack',name:'Jack'},{key:2,val:'test',name:'Test'},{key:3,val:'richie',name:'Richie'}];
        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">选择图片</div>
            </div>
        );

        return (
            <div>
                <Form className="ant-advanced-search-form" onSubmit={()=>this.handleSearch}>
                    <h3>基本信息</h3>
                    <Divider dashed/>
                    <Row gutter={16}>
                        <Col span={12}>
                            <FormItem label="文章标题">
                                {getFieldDecorator('title')(
                                    <Input placeholder="文章标题" />
                                )}


                            </FormItem>
                        </Col>
                        <Col span={6}>
                            <FormItem label="所属频道">
                                {getFieldDecorator('channel')(
                                    <Selection msg={this.props.channelMap} dv=""/>
                                )}

                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={6}>
                            <FormItem label="来源类型">
                                {getFieldDecorator('sourceType')(
                                    <Selection  msg={this.props.sourceMap} dv=""/>
                                )}
                            </FormItem>
                        </Col>
                        <Col span={6}>
                            <FormItem label="来源名称">
                                {getFieldDecorator('sourceName')(
                                    <Input placeholder="文章来源" />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <FormItem label="来源URL">
                                {getFieldDecorator('sourceUrl')(
                                    <Input placeholder="来源URL" />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <FormItem label="来源发布时间">
                                {getFieldDecorator('sourceReleaseTime')(
                                    <DatePicker/>
                                )}

                            </FormItem>
                        </Col>
                    </Row>
                    <br/>
                    <h3>文章内容</h3>
                    <Divider dashed/>
                    <Row gutter={16}>
                        <Col  span={12}>
                            <FormItem label="封面类型:">
                                {getFieldDecorator('coverType',{initialValue:"1"})(
                                    <RadioGroup>
                                    <Radio value="1">左图</Radio>
                                    <Radio value="2">三图</Radio>
                                    <Radio value="3">大图</Radio>
                                    <Radio value="4">无图</Radio>
                                    </RadioGroup>
                                )}

                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col  span={20}>
                            <FormItem label="文章封面:" className="clearfix">
                                {getFieldDecorator('cover')(
                                    <Upload
                                        action={this.props.upLoadCover}
                                        listType="picture-card"
                                        fileList={fileList}
                                        onPreview={this.handlePreview}
                                        onChange={this.handleChange}
                                        accept=""
                                    >
                                        {fileList.length >= 3 ? null : uploadButton}
                                    </Upload>
                                )}
                                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                    <img alt="预览" title="默认封面" style={{ width: '100%' }} src={previewImage} />
                                </Modal>

                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col  span={20}>
                            <FormItem label="内容：　　">
                                <Editor getHtml={this.props.getHtml}  disabled={false} useButton={true} data=""/>
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={20}>
                            <FormItem label="标签:　　">
                                {/*<Tags/>*/}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>


            </div>
        )
    }
}

export default AppComponent;