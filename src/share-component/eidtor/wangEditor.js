/**
 * Created by Richie on 2018/4/11
 */
import React, {Component} from 'react';
import {Button} from 'antd';
const xss = require('xss');
const EEditor =  require('wangeditor');
const editor =  new EEditor('#editor');

class WangEditor extends Component {
    constructor(props) {
        super(props);
        this.state={
            data:''
        }
    }

    componentDidMount() {
        editor.customConfig.menus = this.props.disabled?[]:[
            'head',  // 标题
            'bold',  // 粗体
            'fontSize',  // 字号
            'fontName',  // 字体
            'italic',  // 斜体
            'underline',  // 下划线
            'strikeThrough',  // 删除线
            'foreColor',  // 文字颜色
            'backColor',  // 背景颜色
            'link',  // 插入链接
            'list',  // 列表
            'justify',  // 对齐方式
            'quote',  // 引用
            'emoticon',  // 表情
            'image',  // 插入图片
            'table',  // 表格
            'undo',  // 撤销
            'redo'  // 重复
        ];
        editor.customConfig.uploadImgShowBase64 = true;
        editor.create();
        if(this.props.data!==""){
            editor.txt.html(this.props.data)
        }
        if(this.props.disabled){
            editor.$textElem.attr('contenteditable', false)//禁用编辑
        }
    }


    // //获取富文本内容并进行xss过滤
    handXssHtml =() =>(xss(editor.txt.html()));

    render() {

        return (
            <div>
                <div id="editor">
                </div>
                {this.props.useButton?(
                    <Button  type="primary" onClick={()=>this.props.getHtml(this.handXssHtml())}>保存</Button>
                ):("")}
            </div>
        )
    }
}

export default WangEditor;
