/**
 * Created by Richie on 2018/4/23
 */
import React, {Component} from 'react';


class AppComponent extends Component {
    constructor(props){
        super(props);

    }

    componentDidMount(){
        // const imgArr = Array.prototype.slice.call(this.refs.test.getElementsByTagName("img"));
        // const width = this.refs.test.offsetWidth;
        // if (this.props.isPhone){
        //     imgArr.forEach((v)=>{
        //         v.width = (v.width>300)?300:v.width
        //     })
        // }else {
        //     imgArr.forEach((v)=>{
        //         v.width=(v.width>0.7*width)?width:v.width;
        //     })
        // }

    }

    render() {
        return (
            <div>
                <div ref="test" className="test" dangerouslySetInnerHTML={{__html:this.props.data}}  style={{"overflowY":"auto","overflowX":"auto","width":"100%","border":"0","height":"600px"}}>
                </div>
            </div>
        )
    }
}

export default AppComponent;
