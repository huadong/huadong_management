import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Layout, Menu, Icon, Breadcrumb, Dropdown, Button} from 'antd';
import getLocation from '../../utils/location';
import {logout, getSession, getStorage} from "../../utils/auth";
import msg from '../../utils/msg';
import config from '../../config';
import {withRouter} from 'react-router-dom';
import {Redirect} from 'react-router-dom';
const {SubMenu} = Menu;
const {Header, Sider, Footer} = Layout;


function safeLogout() {

}


class LayoutComponent extends Component {

    constructor(props){
        super(props);

    }
    handleLogout =() => {
        msg(config.LOAD,"登出中");
        setTimeout(this.handleSafeLogout,900)

    };
    handleSafeLogout = () =>{
        logout();
        this.props.history.replace('/login');
    };

    handleMenu = (params) => {
        const array = [];
        for (let o in params) {
            const menu = [];
            debugger;
            if (!params[o].allowed) {
                continue;
            }
            switch (params[o].menuName) {
                case "内容管理":
                    for (let item in params[o].child) {
                        if (!params[o].child[item].allowed) {
                            continue;
                        }
                        switch (params[o].child[item].menuName) {
                            case "线上内容管理":
                                menu.push(
                                    <Menu.Item key="1"><Link to="/">线上内容管理</Link></Menu.Item>
                                );
                                break;
                            case "网络文章初审":
                                menu.push(
                                    <Menu.Item key="2"><Link to="/first-check">网络文章初审</Link></Menu.Item>
                                );
                                break;
                            case "网络文章复审":
                                menu.push(
                                    <Menu.Item key="3"><Link to="/repeat-check">网络文章复审</Link></Menu.Item>
                                );
                                break;
                        }
                    }
                    array.push(
                        <SubMenu key="sub1" title={<span><Icon type="bars"/>内容管理</span>}>
                            {menu}
                        </SubMenu>
                    )
                    break;
                case "内容安全":
                    for (let item in params[o].child) {
                        if (!params[o].child[item].allowed) {
                            continue;
                        }
                        switch (params[o].child[item].menuName) {
                            case "敏感词管理":
                                menu.push(
                                    <Menu.Item key="4"><Link to="/sensitive-words">敏感词管理</Link></Menu.Item>
                                );
                                break;
                        }
                    }
                    array.push(
                        <SubMenu key="sub2" title={<span><Icon type="lock"/>内容安全</span>}>
                            {menu}
                        </SubMenu>
                    )
                    break;
                case "平台管理":
                    for (let item in params[o].child) {
                        if (!params[o].child[item].allowed) {
                            continue;
                        }
                        switch (params[o].child[item].menuName) {
                            case "审核记录查询":
                                menu.push(
                                    <Menu.Item key="5"><Link to="/record">审核记录查询</Link></Menu.Item>
                                );
                                break;
                            case "审核成员管理":
                                menu.push(
                                    <Menu.Item key="6"><Link to="/member">审核成员管理</Link></Menu.Item>
                                );
                                break;
                        }
                    }
                    array.push(
                        <SubMenu key="sub3" title={<span><Icon type="desktop"/>平台管理</span>}>
                            {menu}
                        </SubMenu>
                    )
                    break;
                default:
                    break;
            }
        }
        return array;
    };

    handleMenuPersonal = (params) => {
        const array = [];
        for (let o in params) {
            const menu = [];
            if (!params[o].allowed) {
                continue;
            }
            switch (params[o].menuName) {
                case "管理设置":
                    for (let item in params[o].child) {
                        if (!params[o].child[item].allowed) {
                            continue;
                        }
                        switch (params[o].child[item].menuName) {
                            case "个人中心":
                                menu.push(
                                    <Menu.Item key="7"><Link to="/personal-center/:id">个人中心</Link></Menu.Item>
                                );
                                break;
                            case "角色管理":
                                menu.push(
                                    <Menu.Item key="8"><Link to="/char-mana">角色管理</Link></Menu.Item>
                                );
                                break;
                            case "成员管理":
                                menu.push(
                                    <Menu.Item key="9"><Link to="/mem-mana">成员管理</Link></Menu.Item>
                                );
                                break;
                        }
                    }
                    array.push(
                        <SubMenu key="sub1" title={<span><Icon type="bars"/>管理设置</span>}>
                            {menu}
                        </SubMenu>
                    )
                    break;
                default:
                    break;
            }
        }
        return array;
    }

    render() {
        const local = getLocation(window.location.href);
        const selectKey = [];
        const id = (!!getSession()) ? getSession().userDTO.id : "";
        const userMenu = (!!getStorage("menu")) ? getStorage("menu") : [];
        console.log(userMenu);
        const url = '/personal-center/' + id;
        const menu = [];
        for (let o in userMenu) {
            console.log(userMenu[o]);
            switch (userMenu[o].menuName) {
                case "管理设置":
                    for (let item in userMenu[o].child) {
                        console.log(userMenu[o].child[item]);
                        if (!userMenu[o].child[item].allowed) {
                            continue;
                        }
                        switch (userMenu[o].child[item].menuName) {
                            case "个人中心":
                                menu.push(
                                    <Menu.Item key={userMenu[o].child[item].menuName}>
                                        <Link to={url}>个人中心</Link>
                                    </Menu.Item>
                                );
                                break;
                            case "角色管理":
                                menu.push(
                                    <Menu.Item key={userMenu[o].child[item].menuName}>
                                        <Link to='/char-mana'>角色管理</Link>
                                    </Menu.Item>
                                );
                                break;
                            case "成员管理":
                                menu.push(
                                    <Menu.Item key={userMenu[o].child[item].menuName}>
                                        <Link to='/mem-mana'>成员管理</Link>
                                    </Menu.Item>
                                );
                                break;
                        }
                    }
            }
        }
        menu.push(
            <Menu.Item key="logout">
                <a type="primary" onClick={this.handleLogout}>登出</a>
            </Menu.Item>
        )
        // const menu = (
        //     <Menu>
        //         <Menu.Item>
        //             <Link to={url}>个人中心</Link>
        //         </Menu.Item>
        //         <Menu.Item>
        //             <Link to='/char-mana'>角色管理</Link>
        //         </Menu.Item>
        //         <Menu.Item>
        //             <Link to='/mem-mana'>成员管理</Link>
        //         </Menu.Item>
        //         <Menu.Item>
        //             <a type="primary" onClick={this.handleLogout}>登出</a>
        //         </Menu.Item>
        //     </Menu>
        // );
        selectKey[0] = local.key;
        const openKey = [];
        openKey[0] = local.menu;
        const menuAll = (
            <Menu>
                {menu}
            </Menu>
        )
        return (
            <div>
                <Layout>
                    <Header className="header" style={{
                        background: '#FFFFFF',
                        color: '#FFFFFF',
                        fontSize: '25px',
                        height: '60px',
                        cursor: 'pointer',
                        verticalAlign: 'middle'
                    }}>
                        <div style={{"display":"flex","justifyContent":"space-between"}}>
                            <Link to='/'> <span> <Icon type="mail"/>&nbsp;内容审核管理后台</span></Link>
                            <Dropdown overlay={menuAll}><Button style={{"marginTop":"15px"}}>{(!!getSession())?getSession().userDTO.realName:"用户名"}</Button></Dropdown>
                        </div>
                    </Header>

                    <Layout>
                        <Sider width={200} style={{background: '#fff'}}>
                            {
                                local.menu === "sub1" || local.menu === "sub2" || local.menu === "sub3" ? (
                                        <Menu
                                            mode="inline"
                                            defaultSelectedKeys={selectKey}
                                            defaultOpenKeys={openKey}
                                            style={{height: '100%', borderRight: 0}}
                                        >
                                            {
                                                this.handleMenu(userMenu)
                                            }
                                            {/*<SubMenu key="sub1" title={<span><Icon type="bars"/>内容管理</span>}>*/}
                                            {/*<Menu.Item key="1"><Link to="/">线上内容管理</Link></Menu.Item>*/}
                                            {/*<Menu.Item key="2"><Link to="/first-check">网络文章初审</Link></Menu.Item>*/}
                                            {/*<Menu.Item key="3"><Link to="/repeat-check">网络文章复审</Link></Menu.Item>*/}
                                            {/*</SubMenu>*/}
                                            {/*<SubMenu key="sub2" title={<span><Icon type="lock"/>内容安全</span>}>*/}
                                            {/*<Menu.Item key="4"><Link to="/sensitive-words">敏感词管理</Link></Menu.Item>*/}
                                            {/*</SubMenu>*/}
                                            {/*<SubMenu key="sub3" title={<span><Icon type="desktop"/>平台管理</span>}>*/}
                                            {/*<Menu.Item key="5"><Link to="/record">审核记录查询</Link></Menu.Item>*/}
                                            {/*<Menu.Item key="6"><Link to="/member">审核成员管理</Link></Menu.Item>*/}
                                            {/*</SubMenu>*/}
                                        </Menu>
                                    ) :
                                    (
                                        <Menu
                                            mode="inline"
                                            defaultSelectedKeys={selectKey}
                                            defaultOpenKeys={openKey}
                                            style={{height: '100%', borderRight: 0}}
                                        >
                                            {
                                                this.handleMenuPersonal(userMenu)
                                            }
                                            {/*<SubMenu key="sub4" title={<span><Icon type="bars"/>管理设置</span>}>*/}
                                            {/*<Menu.Item key="7"><Link*/}
                                            {/*to="/personal-center/:id">个人中心</Link></Menu.Item>*/}
                                            {/*<Menu.Item key="8"><Link to="/char-mana">角色管理</Link></Menu.Item>*/}
                                            {/*<Menu.Item key="9"><Link to="/mem-mana">成员管理</Link></Menu.Item>*/}
                                            {/*</SubMenu>*/}
                                        </Menu>
                                    )
                            }
                        </Sider>
                        <Layout style={{padding: '0 24px 24px'}}>
                            <Breadcrumb style={{margin: '16px 0'}} separator=">">
                                {
                                    !!local.bread?local.bread.map((item, index) => (
                                        <Breadcrumb.Item key={index}>
                                            <Link to={item.route}>{item.name}</Link>
                                        </Breadcrumb.Item>)):"请重新登录"
                                }
                            </Breadcrumb>
                            {this.props.children}
                            {/*<Footer style={{textAlign: 'center'}}>*/}
                                {/*HuaDongCMS ©2018 Created by HuaDong*/}
                            {/*</Footer>*/}
                        </Layout>
                    </Layout>
                </Layout>
            </div>
        )
    }
};


export default withRouter(LayoutComponent);