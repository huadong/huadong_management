import React from 'react';
import {Button} from 'antd';
import {withRouter} from 'react-router-dom';

class LinkButton extends React.Component {

    handleLink = () => {
        const {pathName} = this.props;
        const path = pathName + '/' + this.props.id;
        // this.props.history.push(path);
        this.props.history.push(path);
        // this.props.history.push({ pathname : '/record-history' ,query : { day: 'Friday'} })
    }

    render() {
        const {title, style, type, disabled, size} = this.props;

        return (
            <Button onClick={this.handleLink} size={size} disabled={disabled} style={style} type={type}>
                {title}
            </Button>
        )
    }
}



export default withRouter(LinkButton);
