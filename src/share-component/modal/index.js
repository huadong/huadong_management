/**
 * Created by Richie on 2018/4/9
 */
import React,{Component}from 'react';
import { Modal, Button } from 'antd';

class ConfirmModal extends Component {
    state = {
        visible: false,
        confirmLoading: false,
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        //回调成功
        setTimeout(() => {
            this.props.onConfirm();
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 700);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    render() {
        const { visible, confirmLoading } = this.state;
        return (
            <div>
                <Button type={this.props.type} disabled={this.props.disabled} style={this.props.style?this.props.style:{}} size={this.props.size?this.props.size:"default"} onClick={this.showModal}>{this.props.btn}</Button>
                <Modal title={this.props.title}
                       visible={visible}
                       cancelText="取消"
                       okText="确认"
                       okType={this.props.okType}
                       onOk={this.handleOk}
                       confirmLoading={confirmLoading}
                       onCancel={this.handleCancel}
                >
                    <p>{this.props.content}</p>
                </Modal>
            </div>
        );
    }
}

export default ConfirmModal;