import React from 'react';
import {Button, Modal} from 'antd';


export default class MultiButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        }
    }

    handleOpenModal = () => {
        this.setState({
            visible: true,
        })
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        })
    }

    handleOk = () => {
        this.props.handleOk();
        this.handleCancel();
    }

    render() {
        const {title, type, modalTitle, modalText, loading, select, children, center, width, size} = this.props;
        return(
            <span>
                <Button type={type} size={size} onClick={this.handleOpenModal}>
                    {title}
                </Button>
                <Modal
                    title={modalTitle}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    width={width}
                    footer={(
                        <div style={center ? {textAlign: "center"} : null}>
                                <Button key="back" onClick={this.handleCancel}>取消</Button>
                                <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
                                    确定
                                </Button>
                        </div>
                    )}
                >
                    {select ?
                        children
                     : (
                        <p>
                            {modalText}
                        </p>
                    )}
                </Modal>
            </span>

        )
    }

}