/**
 * Created by Richie on 2018/4/16
 */
import React from 'react';
import PropTypes from 'prop-types';

import {Button} from 'antd';
import {withRouter} from 'react-router-dom';

class PushUrl extends React.Component {

    handleClick = () => {
        this.props.history.push(this.props.path+"/"+this.props.id);
    };

    render() {
        const {title, style, type,size} = this.props;

        return (
            <Button size={size} onClick={this.handleClick} style={style} type={type}>
                {title}
            </Button>
        )
    }
}

PushUrl.propTypes={
    path:PropTypes.string.isRequired,
    id:PropTypes.number.isRequired
};

export default withRouter(PushUrl);
