/**
 * Created by Richie on 2018/4/10
 */
import React, {Component} from 'react';

import { Tag, Tooltip, Button } from 'antd';

class EditableTagGroup extends React.Component {
    state = {
        tags: this.props.tag,
        closable:false,
        btn:"标签管理"
    };

    handleClose = (removedTag) => {
        const tags = this.state.tags.filter(tag => tag !== removedTag);
        this.props.onChange(tags);
        this.setState({ tags });
    };

    handleShowClose = (e) => {
        e.preventDefault();
        const closable = !this.state.closable;
        const btn = closable?"完成管理":"标签管理";
        this.setState({ closable ,btn});

    };

    render() {
        const { tags, inputVisible, inputValue } = this.state;
        return (
            <div>
                {tags.map((tag, index) => {
                    const isLongTag = tag.length > 20;
                    const tagElem = (
                        <Tag key={tag} closable={this.state.closable} afterClose={() => this.handleClose(tag)} >
                            {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                        </Tag>
                    );
                    return isLongTag ? <Tooltip title={tag} key={tag}>{tagElem}</Tooltip> : tagElem;
                })}
                <Button type="primary" onClick={this.handleShowClose}>{this.state.btn}</Button>
            </div>
        );
    }
}

export default EditableTagGroup;