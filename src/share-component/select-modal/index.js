/**
 * Created by Richie on 2018/4/9
 */
/**
 * Created by Richie on 2018/4/9
 */
import React,{Component}from 'react';
import { Modal, Button } from 'antd';

import Selection from '../select';

class SelectModal extends Component {
    state = {
        visible: false,
        confirmLoading: false,
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        setTimeout(() => {
            this.props.onConfirm(this.props.selectType);
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 700);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    render() {
        const { visible, confirmLoading, ModalText } = this.state;
        return (
            <div>
                <Button type={this.props.type} disabled={this.props.disabled} onClick={this.showModal}>{this.props.btn}</Button>
                <Modal title={this.props.title}
                       visible={visible}
                       cancelText="取消"
                       okText="确认"
                       onOk={this.handleOk}
                       confirmLoading={confirmLoading}
                       onCancel={this.handleCancel}
                >
                    <div style={{display:"flex",alignItems:"center"}}>
                        <div>{this.props.content}</div>
                        <Selection msg={this.props.msg} dv={this.props.dv} onChange={this.props.selectChange}/>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default SelectModal;