/**
 * Created by Richie on 2018/4/9
 */
import React, {Component} from 'react';
import {Select} from 'antd';

const Option = Select.Option;
class Selection extends Component{

    render(){
        const arr  = this.props.msg;
        return(
            <div>
                <Select defaultValue={this.props.dv} style={{ width: 120 }} onChange={(value)=>this.props.onChange(value)}>
                    {
                        arr.map((item,index)=>(
                            <Option key={item.id} value={item.val}>{item.name}</Option>
                        ))
                    }
                </Select>
            </div>
        )
    }

};
export default Selection;