/**
 * Created by Richie on 2018/4/11
 */
import React, {Component} from 'react';
import {Modal} from 'antd';
import defaultImage from '../../static/default.png';
class ShowImages extends Component {
    constructor(props) {
        super(props);
        this.state={
            previewVisible: false,
            previewImage: '',
            images:this.props.list.length!==0?this.props.list:[defaultImage]
        }
    }


    componentWillUpdate(nextProps,nextState){
        if(this.props.list!==nextProps.list&&nextProps.list.length!==0){
            this.setState({
                images:nextProps.list
            })
        }
    }

    handleCancel = () => this.setState({ previewVisible: false })

    //图片预览
    handlePreview = (item,index) => {
        console.log(item,index)
        this.setState({
            previewImage: item,
            previewTitle:"封面"+(index+1),
            previewVisible: true,
        });
    }


    render() {
        const { previewVisible, previewImage, images, previewTitle } = this.state;
        return (
            <div style={{display:"inline-block"}}>
                {
                    images.map((item,index)=>(
                        <img src={item} alt={"封面"+index+1} title={"封面"+(index+1)} style={this.props.style} onClick={()=>this.handlePreview(item,index)}/>
                    ))
                }
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="预览" title={previewTitle} style={{ width: '100%' }} src={previewImage} />
                </Modal>
            </div>
        )
    }
}

export default ShowImages;
