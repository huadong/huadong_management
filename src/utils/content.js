/**
 * Created by Richie on 2018/4/9
 */
import React from 'react';
import {Layout} from 'antd';

const {Content}=Layout;

 export default  (Component)=>(
     <Content style={{ background: '#fff', padding: 24, margin: 0 }}>
         {Component}
     </Content>
 )